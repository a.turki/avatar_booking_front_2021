import {Component, OnDestroy, OnInit} from '@angular/core';
import {MissionService} from '../services/mission.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {TokenStorageService} from '../services/token-storage.service';
import {NotificationService} from '../services/notification.service';
import {Notification} from '../models/notification';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit, OnDestroy {
  notifications: Notification[];
  username: string;
  email: string;
  isLoggedIn = false;
  // pagination
  page = 1;
  count = 0;
  tableSize = 4;
  tableSizes = [ 4, 8, 12];

  // tslint:disable-next-line:max-line-length
  constructor(private missionService: MissionService, private notificationService: NotificationService, private router: Router, public dialog1: MatDialog, private tokenStorageService: TokenStorageService) { }

  ngOnDestroy(): void {
    for (const n of this.notifications) {
      console.log(n);
      if (!n.seen){
        this.notificationService.checkNotification(n.id).subscribe(data => {
          console.log('seen'); });
      }

    }
    }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (!this.isLoggedIn){ this.router.navigate(['/home']); }
    if (this.isLoggedIn) {
      this.username = this.tokenStorageService.getUser().username;
      this.email = this.tokenStorageService.getUser().email;
      this.getMyNotifications();
    }


  }

  getMyNotifications(): void {
    this.notificationService.getMyNotifications(this.email).subscribe(data => {
      this.notifications = data;
    });
  }

  // tslint:disable-next-line:typedef
  onTableDataChange(event){
    this.page = event;
    this.getMyNotifications();
  }

  onTableSizeChange(event): void {
    this.tableSize = event.target.value;
    this.page = 1;
    this.getMyNotifications();
  }


}
