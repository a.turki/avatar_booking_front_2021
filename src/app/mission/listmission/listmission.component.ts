import { Component, OnInit } from '@angular/core';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {Mission} from '../../models/mission';
import {MatDialog} from '@angular/material/dialog';
import {MyDialogModalComponent} from '../../modals/my-dialog-modal/my-dialog-modal.component';
import {MyDeleteModalsComponent} from '../../modals/my-delete-modals/my-delete-modals.component';
import {TokenStorageService} from '../../services/token-storage.service';
import {MissionDetailsComponent} from '../../modals/mission-details/mission-details.component';
import {RatingModalsComponent} from '../../modals/rating-modals/rating-modals.component';
import {SpecificMissionModalComponent} from '../../modals/specific-mission-modal/specific-mission-modal.component';
import {BoadcastMissionModalComponent} from '../../modals/boadcast-mission-modal/boadcast-mission-modal.component';
import {ReclamationModalsComponent} from '../../modals/reclamation-modals/reclamation-modals.component';
import {ReclamationService} from '../../services/reclamation.service';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-listmission',
  templateUrl: './listmission.component.html',
  styleUrls: ['./listmission.component.css']
})
export class ListmissionComponent implements OnInit {

  // tslint:disable-next-line:max-line-length
  constructor(private reclamationService: ReclamationService, private missionService: MissionService, private router: Router, public dialog1: MatDialog, private tokenStorageService: TokenStorageService) { }
  missions: Mission[];
  ClientEmail: string;
  // mod
  dialogValue1: string;
  sendValue1: string;
  isOwner = false;
  ownerMail: string;
  isLoggedIn = false;
  // pagination
  page = 1;
  count = 0;
  tableSize = 8;
  tableSizes = [ 8, 12, 16];


  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (!this.isLoggedIn){ this.router.navigate(['/home']); }

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.ownerMail = user.email;
      this.ClientEmail = user.email;
      this.getMyMissions(user.email);
    }


    // ***
    // this.getMissions();
  }
  private getMissions(): void {
    this.missionService.getMissionsList().subscribe(data => {
      this.missions = data;
    });
  }
  getMyMissions(clientMail: string): void {
    this.missionService.getMyMissionsList(clientMail).subscribe(data => {
      this.missions = data;
    });
  }
  VerifyProp(clientMail: string): boolean {
    if (this.ownerMail === clientMail)
    {
      // console.log('is owner true');
      return true;
    }

  }
  toRate(mission: Mission): boolean {
    if ((mission.progress === 'done') && (mission.state === 'accepted'))
    {
     // console.log('is ready to rate');
      return true;
    }

  }

  isUndone(mission: Mission): boolean {

    if (mission.progress === 'undone'){
      return true;
    }

  }

  // isRated(mission: Mission): boolean {
  //
  //   return this.missionService.rated(mission.id);
  //
  // }

  isDone(mission: Mission): boolean {

    if (mission.progress === 'done'){
      return true;
    }

  }
  isDoing(mission: Mission): boolean {

    if (mission.progress === 'doing'){
      return true;
    }

  }
  isAccepted(mission: Mission): boolean {

    if (mission.state === 'accepted'){
      return true;
    }

  }
  isPending(mission: Mission): boolean {

    if (mission.state === 'pending'){
      return true;
    }

  }
  isPendingProgress(mission: Mission): boolean {

    if (mission.progress === 'pending'){
      return true;
    }

  }

  alreadyClaimed(m: Mission): boolean{

    return m.claimed.includes(this.tokenStorageService.getUser().email);



  }



deleteMission( id: string): void {
    this.missionService.deleteMissionParId(id).subscribe(data => {
      console.log(data);
      this.getMissions(); });

  }
openDialogD(id: string): void{
    const dialogRef = this.dialog1.open(MyDeleteModalsComponent,
      {
        disableClose: true,
        width: '400px',
        data: { pageValue2: id }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });
    // dialogRef.afterClosed().subscribe(()=>);

    // ...
  }
openDialogDetails(id: string): void{
    const dialogRef = this.dialog1.open(MissionDetailsComponent,
      {
        disableClose: true,
        height: '350px' ,
        width: '550px',
        data: { pageValue3: id }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }
openDialogRating(id: string): void{
    const dialogRef = this.dialog1.open(RatingModalsComponent,
      {
        panelClass: 'dialog-container-custom',
        disableClose: true,
        width: '370px',
        data: { pageValue3: id }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }

  // tslint:disable-next-line:typedef
onTableDataChange(event){
    this.page = event;
    this.getMissions();
  }

onTableSizeChange(event): void {
    this.tableSize = event.target.value;
    this.page = 1;
    this.getMissions();
  }

openDialogBroadcastMission(): void{
    const dialogRef = this.dialog1.open(BoadcastMissionModalComponent,
      {
        disableClose: true,
        width: '450px',
        maxHeight: '726px'
      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }

openDialogClaim(id: string): void{
    const dialogRef = this.dialog1.open(ReclamationModalsComponent,
      {
        panelClass: 'dialog-container-custom',
        disableClose: true,
        width: '500px',
        data: { pageValue3: id }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }



}
