import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificAvatarcardPhotoComponent } from './specific-avatarcard-photo.component';

describe('SpecificAvatarcardPhotoComponent', () => {
  let component: SpecificAvatarcardPhotoComponent;
  let fixture: ComponentFixture<SpecificAvatarcardPhotoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecificAvatarcardPhotoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificAvatarcardPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
