import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionAvatarsComponent } from './mission-avatars.component';

describe('MissionAvatarsComponent', () => {
  let component: MissionAvatarsComponent;
  let fixture: ComponentFixture<MissionAvatarsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MissionAvatarsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionAvatarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
