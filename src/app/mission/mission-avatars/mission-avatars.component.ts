import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {MissionService} from '../../services/mission.service';
import {MatDialog} from '@angular/material/dialog';
import {Mission} from '../../models/mission';
import {User} from '../../models/user';
import {MyDialogModalComponent} from '../../modals/my-dialog-modal/my-dialog-modal.component';
import {RatingModalsComponent} from '../../modals/rating-modals/rating-modals.component';
import {AvatarRatesModalsComponent} from '../../modals/avatar-rates-modals/avatar-rates-modals.component';

@Component({
  selector: 'app-mission-avatars',
  templateUrl: './mission-avatars.component.html',
  styleUrls: ['./mission-avatars.component.css']
})
export class MissionAvatarsComponent implements OnInit {
  //id: string;
  @Input() id: string;
  mission: Mission;
  avatars: User[];
  candidats: string[];
  // for modal
  dialogValue: string;
  sendValue: string;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private route: ActivatedRoute, private token: TokenStorageService, private missionService: MissionService, public dialog: MatDialog, public dialog1: MatDialog) { }


  ngOnInit(): void {
   // this.id = this.route.snapshot.params.id;
    this.mission = new Mission();
    this.missionService.getMissionAvatarsList(this.id).subscribe(data => {
      this.avatars = data;

    });
  }
  openDialog(id: string): void {
    const dialogRef = this.dialog.open(MyDialogModalComponent, {
      width: '400px',
      backdropClass: 'custom-dialog-backdrop-class',
      panelClass: 'custom-dialog-panel-class',
      data: { pageValue1: id }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.dialogValue = result.data;
    });
  }
  openDialogRates(avatarmail: string): void{
    const dialogRef = this.dialog1.open(AvatarRatesModalsComponent,
      {
        disableClose: true,
        data: { pageValue3: avatarmail }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }

}
