import {Component, Input, OnInit} from '@angular/core';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {RatingService} from '../../services/rating.service';
import {Mission} from '../../models/mission';
import {Rating} from '../../models/rating';

@Component({
  selector: 'app-rate-mission',
  templateUrl: './rate-mission.component.html',
  styleUrls: ['./rate-mission.component.css']
})
export class RateMissionComponent implements OnInit {
  @Input() missiondid: string;
  client: string;
  mission: Mission = new Mission();
  rating: Rating = new Rating();
  isLoggedIn = false;
  // tslint:disable-next-line:max-line-length
  constructor(private missionservice: MissionService, private ratingservice: RatingService, private router: Router, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      this.client = this.tokenStorageService.getUser().email;

      this.missionservice.getMissionById(this.missiondid).subscribe(data => {
        this.mission = data;
      });
    }
  }

  rateMission(): void{

    // this.missionservice.createMission(this.mission).subscribe(data => {console.log(data); }, error => console.log(error));
   // this.goToMissionList();

    this.ratingservice.rateMission(this.rating, this.missiondid)
                      .subscribe(data => {console.log(data); }, error => console.log(error));
    this.router.navigate(['/allmissions']);
    window.location.reload();

  }

}
