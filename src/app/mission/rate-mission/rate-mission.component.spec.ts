import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RateMissionComponent } from './rate-mission.component';

describe('RateMissionComponent', () => {
  let component: RateMissionComponent;
  let fixture: ComponentFixture<RateMissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RateMissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RateMissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
