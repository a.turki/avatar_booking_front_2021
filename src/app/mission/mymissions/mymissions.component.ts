import { Component, OnInit } from '@angular/core';
import {Mission} from '../../models/mission';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {MyDeleteModalsComponent} from '../../modals/my-delete-modals/my-delete-modals.component';
import {TokenStorageService} from '../../services/token-storage.service';
import {MissionDetailsComponent} from '../../modals/mission-details/mission-details.component';

@Component({
  selector: 'app-mymissions',
  templateUrl: './mymissions.component.html',
  styleUrls: ['./mymissions.component.css']
})
export class MymissionsComponent implements OnInit {
  missions: Mission[];
  // mod
  dialogValue1: string;
  sendValue1: string;
  // **
  isLoggedIn = false;
  ClientEmail: string;
  // pagination
  page = 1;
  count = 0;
  tableSize = 4;
  tableSizes = [ 4, 8, 12];
  // tslint:disable-next-line:max-line-length
  constructor(private missionService: MissionService, private router: Router, public dialog1: MatDialog, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.getMyMissions(user.email);
      this.ClientEmail = user.email;
    }

    console.log('******************************' + this.ClientEmail);
  }

  getMyMissions(clientMail: string): void {
    this.missionService.getMyMissionsList(clientMail).subscribe(data => {
      this.missions = data;
    });
  }

  deleteMission( id: string): void {
    this.missionService.deleteMissionParId(id).subscribe(data => {
      console.log(data);
      this.getMyMissions(this.ClientEmail); });

  }
  openDialogD(id: string): void{
    const dialogRef = this.dialog1.open(MyDeleteModalsComponent,
      {
        disableClose: true,
        width: '400px',
        data: { pageValue2: id }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }
  openDialogDetails(id: string): void{
    const dialogRef = this.dialog1.open(MissionDetailsComponent,
      {
        disableClose: true,
        width: '50%',
        data: { pageValue3: id }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }

  hide(mission: Mission): boolean{
    if (this.isPending(mission) && this.isPendingProgress(mission))
    {
      return true;
    }
  }
  // tslint:disable-next-line:typedef
  onTableDataChange(event){
    this.page = event;
    this.getMyMissions(this.ClientEmail);
  }

  onTableSizeChange(event): void {
    this.tableSize = event.target.value;
    this.page = 1;
    this.getMyMissions(this.ClientEmail);
  }
  VerifyProp(clientMail: string): boolean {
    if (this.ClientEmail === clientMail)
    {
      console.log('is owner true');
      return true;
    }

  }
  toRate(mission: Mission): boolean {
    if ((mission.progress === 'done') && (mission.state === 'accepted'))
    {
      console.log('is ready to rate');
      return true;
    }

  }

  isUndone(mission: Mission): boolean {

    if (mission.progress === 'undone'){
      return true;
    }

  }

  // isRated(mission: Mission): boolean {
  //
  //   return this.missionService.rated(mission.id);
  //
  // }
  isDone(mission: Mission): boolean {

    if (mission.progress === 'done'){
      return true;
    }

  }
  isDoing(mission: Mission): boolean {

    if (mission.progress === 'doing'){
      return true;
    }

  }
  isAccepted(mission: Mission): boolean {

    if (mission.state === 'accepted'){
      return true;
    }

  }
  isPending(mission: Mission): boolean {

    if (mission.state === 'pending'){
      return true;
    }

  }
  isPendingProgress(mission: Mission): boolean {

    if (mission.progress === 'pending'){
      return true;
    }

  }
}
