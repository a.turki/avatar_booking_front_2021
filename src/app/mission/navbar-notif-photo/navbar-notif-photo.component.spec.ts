import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarNotifPhotoComponent } from './navbar-notif-photo.component';

describe('NavbarNotifPhotoComponent', () => {
  let component: NavbarNotifPhotoComponent;
  let fixture: ComponentFixture<NavbarNotifPhotoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarNotifPhotoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarNotifPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
