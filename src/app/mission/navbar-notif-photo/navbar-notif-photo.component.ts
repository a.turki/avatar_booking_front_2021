import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../models/user';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from '../../services/token-storage.service';

@Component({
  selector: 'app-navbar-notif-photo',
  templateUrl: './navbar-notif-photo.component.html',
  styleUrls: ['./navbar-notif-photo.component.css']
})
export class NavbarNotifPhotoComponent implements OnInit {
  @Input() id: string;
  user: User;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  imageName: any;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private route: ActivatedRoute, private userService: UserService, private httpClient: HttpClient, private token: TokenStorageService) { }

  ngOnInit(): void {
    // this.id = this.route.snapshot.params.id;
    this.user = new User();
    // this.userService.getUserById(this.id).subscribe(data => {this.user = data;
    //                                                          this.imageName = this.user.username;
    //                                                          this.getImage(this.imageName); });
    this.userService.getEmployeeByUsername(this.id).subscribe(data => {this.user = data;
      this.imageName = this.user.username;
      this.getImage(this.imageName); });
  }

  // tslint:disable-next-line:typedef
  getImage(s: string) {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    this.httpClient.get('http://localhost:8080/api/get/' + s, { headers: header})
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
  }

}
