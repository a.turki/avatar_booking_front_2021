import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {MissionService} from '../../services/mission.service';
import {Mission} from '../../models/mission';
import {User} from '../../models/user';

@Component({
  selector: 'app-deletemission',
  templateUrl: './deletemission.component.html',
  styleUrls: ['./deletemission.component.css']
})
export class DeletemissionComponent implements OnInit {
  @Input() id: string;
  mission: Mission;
  constructor(private router: Router, private route: ActivatedRoute, private missionService: MissionService) { }

  ngOnInit(): void {
    this.mission = new Mission();
    // this.userService.getUserById(this.id).subscribe(data => {this.user = data; });
    this.missionService.getMissionById(this.id).subscribe(data => {this.mission = data;
      // tslint:disable-next-line:max-line-length
                                                                   console.log('******-*-*--*-*-*-**--*-**--*-*-*-*-*-*-*-*-*-*-*' + this.mission.title);});
    console.log('******-*-*--*-*-*-**--*-**--*-*-*-*-*-*-*-*-*-*-*' + this.mission.title);
  }
  deleteMission( id: string): void {
    this.missionService.deleteMissionParId(id).subscribe(data => {
      console.log(data);
       });
    this.goToMissionList();
    window.location.reload();

  }
  cancelDelete(): void
  {
    this.goToMissionList();
    window.location.reload();
  }

  goToMissionList(): void{
    this.router.navigate(['/allmissions']);

  }

}
