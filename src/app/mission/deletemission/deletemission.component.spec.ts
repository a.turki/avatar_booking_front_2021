import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletemissionComponent } from './deletemission.component';

describe('DeletemissionComponent', () => {
  let component: DeletemissionComponent;
  let fixture: ComponentFixture<DeletemissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeletemissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletemissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
