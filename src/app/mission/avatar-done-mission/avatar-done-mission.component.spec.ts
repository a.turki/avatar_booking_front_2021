import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvatarDoneMissionComponent } from './avatar-done-mission.component';

describe('AvatarDoneMissionComponent', () => {
  let component: AvatarDoneMissionComponent;
  let fixture: ComponentFixture<AvatarDoneMissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvatarDoneMissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarDoneMissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
