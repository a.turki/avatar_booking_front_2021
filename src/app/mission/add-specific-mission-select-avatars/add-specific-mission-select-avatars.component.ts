import { Component, OnInit } from '@angular/core';
import {Mission} from '../../models/mission';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {User} from '../../models/user';
import {RatingModalsComponent} from '../../modals/rating-modals/rating-modals.component';
import {MatDialog} from '@angular/material/dialog';
import {SpecificMissionModalComponent} from '../../modals/specific-mission-modal/specific-mission-modal.component';
import {MyDialogModalComponent} from '../../modals/my-dialog-modal/my-dialog-modal.component';
import {AvatarRatesModalsComponent} from '../../modals/avatar-rates-modals/avatar-rates-modals.component';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-add-specific-mission-select-avatars',
  templateUrl: './add-specific-mission-select-avatars.component.html',
  styleUrls: ['./add-specific-mission-select-avatars.component.css']
})
export class AddSpecificMissionSelectAvatarsComponent implements OnInit {

  mission: Mission = new Mission();
 // client: string;
 // avatars: string[];
  isLoggedIn = false;
  avatars: User[];
  avatarsSelected: string[];
  searchText: string;
  // pagination
  page = 1;
  count = 0;
  tableSize = 4;
  tableSizes = [ 4, 8, 12];
  // tslint:disable-next-line:max-line-length
  constructor(private missionservice: MissionService, private router: Router, public dialog1: MatDialog, private tokenStorageService: TokenStorageService, private httpClient: HttpClient, private token: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (!this.isLoggedIn){ this.router.navigate(['/home']); }
    if (this.isLoggedIn) {
    this.avatarsSelected = [];
    // this.missionservice.getAvatarsList().subscribe(data => { this.avatars = data; });
    this.getAvatars();
    }else if (!this.isLoggedIn)
    {
      this.router.navigate(['/home']);
    }
  }
  getAvatars(): void {
    this.missionservice.getAvatarsList().subscribe(data => { this.avatars = data; });

  }

  // tslint:disable-next-line:typedef
  onTableDataChange(event){
    this.page = event;
    this.getAvatars();
  }

  onTableSizeChange(event): void {
    this.tableSize = event.target.value;
    this.page = 1;
    this.getAvatars();
  }

  goToMissionList(): void{
    this.router.navigate(['/allmissions']);

  }
  saveMission(): void{
    // this.mission.listavatarCandidat=this.missionservice.getAvatarsListStr();
    // this.missionservice.getAvatarsListStr().subscribe(data => {
    //   this.mission.listavatarCandidat = data;
    // });

    this.missionservice.createMission(this.mission).subscribe(data => {console.log(data); }, error => console.log(error));
    this.goToMissionList();
  }
  onSubmit(): void{
    console.log(this.mission);
    /*this.saveEmployee();*/
    this.saveMission();
    this.goToMissionList();
    // tslint:disable-next-line:only-arrow-functions typedef
    setTimeout(function(){ window.location.reload(); }, 1000);


  }
  selectAvatar(avatarmail: string): void{

  this.avatarsSelected.push(avatarmail);
  console.log(this.avatarsSelected);
  }

  VerifySelected(avatarMail: string): boolean {
    if (this.avatarsSelected.includes(avatarMail))
    {
      console.log('verify selected ' + avatarMail + ' true');
      return true;
    }
    else { return false; }

  }

  VerifySelectedEmpty(): boolean {
    if (this.avatarsSelected.length === 0)
    {
      console.log('avatar selected is empty');
      return true;
    }
    else {
      console.log('avatar selected is not empty');
      return false; }

  }

  deselectAvatar(avatarmail: string): void{

    // this.avatarsSelected.push(avatarmail);

   // this.avatarsSelected.ind
    this.avatarsSelected.splice(this.avatarsSelected.indexOf(avatarmail), 1);
    console.log(this.avatarsSelected);
  }
  openDialog(id: string): void {
    const dialogRef = this.dialog1.open(MyDialogModalComponent, {
      width: '350px',
      backdropClass: 'custom-dialog-backdrop-class',
      panelClass: 'custom-dialog-panel-class',
      data: { pageValue1: id }
    });

    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
      dialogRef.afterClosed().subscribe(result => {
        id = result;
      });
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   // console.log('The dialog was closed', result);
    //   // this.dialogValue = result.data;
    //   result.data.pageValue1.clear;
    // });
  }
  openDialogRates(avatarmail: string): void{
    const dialogRef = this.dialog1.open(AvatarRatesModalsComponent,
      {
        disableClose: true,
        data: { pageValue3: avatarmail }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }
  openDialogRating(id: string): void{
    const dialogRef = this.dialog1.open(RatingModalsComponent,
      {
        disableClose: true,
        width: '650px',
        data: { pageValue3: id }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }
  openDialogSpecificMission(selected: string[]): void{
    const dialogRef = this.dialog1.open(SpecificMissionModalComponent,
      {
        disableClose: true,
        width: '450px',
        height: '500px',
        data: { pageValue3: selected }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }

  // getImage(s: string): any{
  //   const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
  //   let retrievedImage: any;
  //   let base64Data: any;
  //   let retrieveResonse: any;
  //   this.httpClient.get('http://localhost:8080/api/get/' + s, { headers: header})
  //     .subscribe(
  //       res => {
  //         retrieveResonse = res;
  //         base64Data = retrieveResonse.picByte;
  //         retrievedImage = 'data:image/jpeg;base64,' + base64Data;
  //         return retrievedImage;
  //       }
  //     );
  //
  // }



}
