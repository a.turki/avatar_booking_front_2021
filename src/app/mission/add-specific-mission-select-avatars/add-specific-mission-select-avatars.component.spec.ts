import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSpecificMissionSelectAvatarsComponent } from './add-specific-mission-select-avatars.component';

describe('AddSpecificMissionSelectAvatarsComponent', () => {
  let component: AddSpecificMissionSelectAvatarsComponent;
  let fixture: ComponentFixture<AddSpecificMissionSelectAvatarsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSpecificMissionSelectAvatarsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSpecificMissionSelectAvatarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
