import { Component, OnInit } from '@angular/core';
import {Mission} from '../../models/mission';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';

@Component({
  selector: 'app-avatar-doing-mission',
  templateUrl: './avatar-doing-mission.component.html',
  styleUrls: ['./avatar-doing-mission.component.css']
})
export class AvatarDoingMissionComponent implements OnInit {
  isLoggedIn = false;
  missions: Mission[];
  AvatarEmail: string;
  constructor(private missionService: MissionService, private router: Router, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.getDoingMissions(user.email);
      this.AvatarEmail = user.email;
    }

    console.log('******************************' + this.AvatarEmail);
  }

  getDoingMissions(avatarMail: string): void {
    this.missionService.getDoingMission(avatarMail).subscribe(data => {
      this.missions = data;
    });
  }

  VerifyIsAvatar(avatarMail: string): boolean {
    if (this.AvatarEmail === avatarMail)
    {
      console.log('is owner true');
      return true;
    }
  }
  isUndone(mission: Mission): boolean {
    if (mission.progress === 'undone' && mission.state === 'accepted')
    {
      return true;
    }

  }

  EndMission(id: string, avatarMail: string): void {
    this.missionService.endMission(id).subscribe(data => {
      console.log(data);
      this.getDoingMissions(avatarMail); }) ;
  }



}
