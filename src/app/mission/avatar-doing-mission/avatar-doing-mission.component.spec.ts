import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvatarDoingMissionComponent } from './avatar-doing-mission.component';

describe('AvatarDoingMissionComponent', () => {
  let component: AvatarDoingMissionComponent;
  let fixture: ComponentFixture<AvatarDoingMissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvatarDoingMissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarDoingMissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
