import {Component, Input, OnInit} from '@angular/core';
import {Mission} from '../../models/mission';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {Notification} from '../../models/notification';
import {Country} from '../../models/country';
import {Region} from '../../models/region';
import {NotificationService} from '../../services/notification.service';
import {ToastService} from '../../services/toast.service';
import {AdminService} from '../../services/admin.service';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-add-specific-mission',
  templateUrl: './add-specific-mission.component.html',
  styleUrls: ['./add-specific-mission.component.css']
})
export class AddSpecificMissionComponent implements OnInit {
  mission: Mission = new Mission();
  mission2: Mission = new Mission();
  client: string;
  currentDate: string;
  Countries: Country[];
  Regions: Region[];
  noAvatorfound = false;
  @Input() avatars: string[];
  notification: Notification = new Notification();
  // tslint:disable-next-line:max-line-length
  constructor(private notificationService: NotificationService, private adminservice: AdminService, private missionservice: MissionService, private router: Router, private tokenStorageService: TokenStorageService, private toastService: ToastService, public dialog1: MatDialog) {
    this.currentDate = new Date().toISOString().slice(0, 16);
  }

  ngOnInit(): void {
    this.client = this.tokenStorageService.getUser().email;
    this.mission.clientemail = this.client;
    this.mission.listavatarCandidat = this.avatars;
    this.getCountries();
}

  goToMissionList(): void{
    this.router.navigate(['/allmissions']);

  }
  saveMission(): void{
    // this.mission.listavatarCandidat=this.missionservice.getAvatarsListStr();
    // this.missionservice.getAvatarsListStr().subscribe(data => {
    //   this.mission.listavatarCandidat = data;
    // });

    this.missionservice.createMission2(this.mission).subscribe(data => {this.mission2 = data;
      // tslint:disable-next-line:max-line-length
                                                                        this.notification.type = 'Added a new Specific mission ' + this.mission.title;
      // tslint:disable-next-line:max-line-length
                                                                        this.notification.senderUsername = this.tokenStorageService.getUser().username;
                                                                        this.notification.missionID = this.mission2.id;
                                                                        console.log('------------------------ ' + this.mission2.id);
      // tslint:disable-next-line:prefer-for-of
                                                                        for (let i = 0; i < this.avatars.length; i++) {
        // console.log(this.avatarsUsername[i]);
        this.notification.recipientUsername = this.avatars[i];
        // tslint:disable-next-line:max-line-length
        this.notificationService.makeNotification(this.notification).subscribe(data1 => {console.log(data1); }, error => console.log(error));


      }

    }, error => console.log(error));
    // this.goToMissionList();


  }
  onSubmit(): void{
    console.log(this.mission);
    /*this.saveEmployee();*/
    this.saveMission();
    this.goToMissionList();
    // window.location.reload();
    this.dialog1.closeAll();
    this.showToasterSuccess();
    // tslint:disable-next-line:only-arrow-functions typedef
   // setTimeout(function(){ window.location.reload(); }, 10);


  }

  // tslint:disable-next-line:typedef
  showToasterSuccess(){

    this.toastService.showSuccess('Mission added successfully !!', this.mission.title);

  }
  getCountries(): void {
    this.adminservice.getCountryList().subscribe(data => {
      this.Countries = data;
    });
  }
  // tslint:disable-next-line:typedef
  onChange($event: any) {
    this.adminservice.getCountryRegionList(this.mission.country).subscribe(data =>
    { this.Regions = data ;
      if (this.mission.listavatarCandidat.length > 0){this.noAvatorfound = false; }
      else {this.noAvatorfound = true ; }

    });
  }

  // tslint:disable-next-line:typedef
  onChangeRegion($event: any) {
    // this.adminservice.getCountryRegionList(this.mission.country).subscribe(data =>
    // { this.Regions = data ; });
    this.missionservice.getAvatarsListStrByRegion(this.mission.region).subscribe(data => {
      this.mission.listavatarCandidat = data;
      if (this.mission.listavatarCandidat.length > 0){this.noAvatorfound = false; }
      else {this.noAvatorfound = true ; }
    } );


  }


}
