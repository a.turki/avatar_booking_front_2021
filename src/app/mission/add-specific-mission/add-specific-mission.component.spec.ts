import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSpecificMissionComponent } from './add-specific-mission.component';

describe('AddSpecificMissionComponent', () => {
  let component: AddSpecificMissionComponent;
  let fixture: ComponentFixture<AddSpecificMissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSpecificMissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSpecificMissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
