import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvatarAllRatingComponent } from './avatar-all-rating.component';

describe('AvatarAllRatingComponent', () => {
  let component: AvatarAllRatingComponent;
  let fixture: ComponentFixture<AvatarAllRatingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvatarAllRatingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarAllRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
