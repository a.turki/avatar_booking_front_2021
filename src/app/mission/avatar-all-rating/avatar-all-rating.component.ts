import {Component, Input, OnInit} from '@angular/core';
import {Mission} from '../../models/mission';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {TokenStorageService} from '../../services/token-storage.service';
import {RatingService} from '../../services/rating.service';
import {Rating} from '../../models/rating';

@Component({
  selector: 'app-avatar-all-rating',
  templateUrl: './avatar-all-rating.component.html',
  styleUrls: ['./avatar-all-rating.component.css']
})
export class AvatarAllRatingComponent implements OnInit {
  missions: Mission[];
  ratings: Rating[];
  // mod
  dialogValue1: string;
  sendValue1: string;
  isOwner = false;
  ownerMail: string;
  isLoggedIn = false;
  norating = false;


  // @Input()
  @Input() avataremail: string;
  // tslint:disable-next-line:max-line-length
  constructor(private missionService: MissionService, private ratingService: RatingService, private router: Router, public dialog1: MatDialog, private tokenStorageService: TokenStorageService) { }


  ngOnInit(): void {
    console.log('norate ' + this.norating );
    this.getMissionsRates(this.avataremail);
    // this.norating =;
    // console.log(this.ratings.length);
    if (this.ratings.length === 0) {
      this.norating = true;
      console.log('norate ' + this.norating );
    }
  }

  getMissionsRates(avatarMail: string): void {
    // this.missionService.getMissionsListOpportunities(avatarMail).subscribe(data => {
    //   this.missions = data;
    // });
    this.ratingService.getAllMissionRate(avatarMail).subscribe(data => {this.ratings = data; });
  }


}
