import {Component, Input, OnInit} from '@angular/core';
import {Mission} from '../../models/mission';
import {User} from '../../models/user';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {MissionService} from '../../services/mission.service';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-mission-information',
  templateUrl: './mission-information.component.html',
  styleUrls: ['./mission-information.component.css']
})
export class MissionInformationComponent implements OnInit {
  @Input() id: string;
  avatars: User[];
  mission: Mission;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private route: ActivatedRoute, private token: TokenStorageService, private missionService: MissionService, public dialog: MatDialog, public dialog1: MatDialog) { }

  ngOnInit(): void {
    this.mission = new Mission();
    this.missionService.getMissionById(this.id).subscribe(data => {this.mission = data; });
  }
  isAccepted(mission: Mission): boolean {

    if (mission.state === 'accepted'){
      return true;
    }

  }
}
