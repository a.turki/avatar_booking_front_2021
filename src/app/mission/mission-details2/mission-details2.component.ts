import {Component, Input, OnInit} from '@angular/core';
import {Mission} from '../../models/mission';
import {User} from '../../models/user';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {MissionService} from '../../services/mission.service';
import {MatDialog} from '@angular/material/dialog';
import {MyDialogModalComponent} from '../../modals/my-dialog-modal/my-dialog-modal.component';
import {MyDeleteModalsComponent} from '../../modals/my-delete-modals/my-delete-modals.component';

@Component({
  selector: 'app-mission-details2',
  templateUrl: './mission-details2.component.html',
  styleUrls: ['./mission-details2.component.css']
})
export class MissionDetails2Component implements OnInit {
  // @Input()
  id: string;
  mission: Mission;
  avatars: User[];
  candidats: string[];
  // for modal
  dialogValue: string;
  sendValue: string;
  // mod
  dialogValue1: string;
  sendValue1: string;

  // DHDHDDH
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private route: ActivatedRoute, private token: TokenStorageService, private missionService: MissionService, public dialog: MatDialog, public dialog1: MatDialog) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.mission = new Mission();
    this.missionService.getMissionById(this.id).subscribe(data => {this.mission = data; });
    this.missionService.getMissionAvatarsList(this.id).subscribe(data => {
      this.avatars = data;

    });
  }
  isOwner() : boolean {
    if (this.mission.clientemail === this.token.getUser().email){
      return true;
    }else { return false; }
  }
  openDialog(id: string): void {
    const dialogRef = this.dialog.open(MyDialogModalComponent, {
      width: '400px',
      backdropClass: 'custom-dialog-backdrop-class',
      panelClass: 'custom-dialog-panel-class',
      data: { pageValue1: id }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.dialogValue = result.data;
    });
  }

  openDialogD(id: string): void{
    const dialogRef = this.dialog1.open(MyDeleteModalsComponent,
      {
        disableClose: true,
        width: '400px',
        data: { pageValue2: id }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }

}
