import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionDetails2Component } from './mission-details2.component';

describe('MissionDetails2Component', () => {
  let component: MissionDetails2Component;
  let fixture: ComponentFixture<MissionDetails2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MissionDetails2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionDetails2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
