import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Addmission2Component } from './addmission2.component';

describe('Addmission2Component', () => {
  let component: Addmission2Component;
  let fixture: ComponentFixture<Addmission2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Addmission2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Addmission2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
