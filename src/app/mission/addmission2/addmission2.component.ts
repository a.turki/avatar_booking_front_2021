import { Component, OnInit } from '@angular/core';
import {Mission} from '../../models/mission';
import {Notification} from '../../models/notification';
import {Country} from '../../models/country';
import {Region} from '../../models/region';
import {AdminService} from '../../services/admin.service';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {NotificationService} from '../../services/notification.service';
import {ToastService} from '../../services/toast.service';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-addmission2',
  templateUrl: './addmission2.component.html',
  styleUrls: ['./addmission2.component.css']
})
export class Addmission2Component implements OnInit {
  mission: Mission = new Mission();
  mission2: Mission = new Mission();
  notification: Notification = new Notification();
  client: string;
  avatars: string[];
  avatarsUsername: string[];
  missionid: string;
  currentDate: string;
  Countries: Country[];
  Regions: Region[];

  noAvatarfound = false;
  // tslint:disable-next-line:max-line-length
  constructor(private adminservice: AdminService, private missionservice: MissionService, private router: Router, private tokenStorageService: TokenStorageService, private notificationService: NotificationService, private toastService: ToastService, public dialog1: MatDialog) {
    this.currentDate = new Date().toISOString().slice(0, 16);
  }

  ngOnInit(): void {

    this.client = this.tokenStorageService.getUser().email;
    this.mission.clientemail = this.client;
    // this.missionservice.getAvatarsListStr().subscribe(data => {
    //   this.mission.listavatarCandidat = data;
    //
    // });
    this.missionservice.getAvatarsListUsernameStr().subscribe(data => {
      this.avatarsUsername = data;
    });
    this.getCountries();
  }
  goToMissionList(): void{
    this.router.navigate(['/allmissions']);

  }
  getCountries(): void {
    this.adminservice.getCountryList().subscribe(data => {
      this.Countries = data;
    });
  }

  saveMission(): void{
    // this.mission.listavatarCandidat=this.missionservice.getAvatarsListStr();
    // this.missionservice.getAvatarsListStr().subscribe(data => {
    //   this.mission.listavatarCandidat = data;
    // });

    // this.missionservice.createMission(this.mission).subscribe(data => {console.log(data); }, error => console.log(error));
    this.missionservice.createMission2(this.mission).subscribe(data => {this.mission2 = data;
      this.missionid = this.mission2.id;
      console.log('mission iddddddddd' + this.missionid);
      // tslint:disable-next-line:max-line-length
      this.notification.type = 'broadcasted a new mission ' + this.mission.title;
      // tslint:disable-next-line:max-line-length
      this.notification.senderUsername = this.tokenStorageService.getUser().username;
      this.notification.missionID = this.mission2.id;
      console.log('------------------------ ' + this.mission2.id);
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.mission.listavatarCandidat.length; i++) {
        // console.log(this.avatarsUsername[i]);
        this.notification.recipientUsername = this.mission.listavatarCandidat[i];
        // tslint:disable-next-line:max-line-length
        this.notificationService.makeNotification(this.notification).subscribe(data1 => {console.log(data1); }, error => console.log(error));


      }
    });

    // this.goToMissionList();



    // this.goToMissionList();
  }
  onSubmit(): void{
    console.log(this.mission);
    /*this.saveEmployee();*/
    this.saveMission();
    this.goToMissionList();
    this.dialog1.closeAll();
    this.showToasterSuccess();

    // this.dialogRef.close();
    // this.dialogRef.afterClosed().subscribe(result => { this.showToasterSuccess(); });


    // console.log(this.missionid);
    // tslint:disable-next-line:only-arrow-functions typedef
    setTimeout(function(){ window.location.reload(); }, 1000);


  }
  // tslint:disable-next-line:typedef
  showToasterSuccess(){

    this.toastService.showSuccess('Mission added successfully !!', this.mission.title);

  }

  // tslint:disable-next-line:typedef
  onChange($event: any) {
    this.adminservice.getCountryRegionList(this.mission.country).subscribe(data =>
    { this.Regions = data ;
      if (this.mission.listavatarCandidat.length > 0){this.noAvatarfound = false; }
      else {this.noAvatarfound = true ; }

    });
  }

  // tslint:disable-next-line:typedef
  onChangeRegion($event: any) {
    // this.adminservice.getCountryRegionList(this.mission.country).subscribe(data =>
    // { this.Regions = data ; });
    this.missionservice.getAvatarsListStrByRegion(this.mission.region).subscribe(data => {
      this.mission.listavatarCandidat = data;
      if (this.mission.listavatarCandidat.length > 0){this.noAvatarfound = false; }
      else {this.noAvatarfound = true ; }
    } );


  }

}
