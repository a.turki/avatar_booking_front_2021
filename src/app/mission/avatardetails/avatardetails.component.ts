import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {MissionService} from '../../services/mission.service';
import {UserService} from '../../services/user.service';
import {Mission} from '../../models/mission';
import {User} from '../../models/user';
import {PhotoService} from '../../services/photo.service';
import {Photo} from '../../models/photo';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {RatingService} from '../../services/rating.service';
import {Rating} from '../../models/rating';

@Component({
  selector: 'app-avatardetails',
  templateUrl: './avatardetails.component.html',
  styleUrls: ['./avatardetails.component.css']
})
export class AvatardetailsComponent implements OnInit {

  @Input() id: string;
  user: User;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  imageName: any;
  avatarMail: string;
  count: number;
  ratings: Rating[];
  finishedMission: Mission[];
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private route: ActivatedRoute, private userService: UserService, private ratingService: RatingService, private missionService: MissionService , private httpClient: HttpClient, private token: TokenStorageService) { }

  ngOnInit(): void {
   // this.id = this.route.snapshot.params.id;
    // let ratingcount: number;
    this.user = new User();
    this.userService.getUserById(this.id).subscribe(data => {this.user = data;
                                                             this.imageName = this.user.username;
                                                             this.getImage(this.imageName);
                                                             this.avatarMail = this.user.email; });
    this.imageName = this.user.username;

    this.getFinishedMissions(this.avatarMail);
    this.getMissionsRates(this.user.email);
    this.count = this.ratings.length;
    this.ratingService.ratingsCount(this.user.email).subscribe(data => {this.count = data; });
    console.log('counttttttttttttttt rtesSSSSSSSSSSSSSSS' + this.ratings.length);

    console.log('*************************d ' + this.user.username);
  }

  // tslint:disable-next-line:typedef
  getImage(s: string) {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    this.httpClient.get('http://localhost:8080/api/get/' + s, { headers: header})
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
  }

  getFinishedMissions(avatarMail: string): number{
    this.missionService.getFinishedMission(avatarMail).subscribe(data => {
      this.finishedMission = data;
    });
    return this.finishedMission.length;
  }

  getMissionsRates(avatarMail: string): void {
    this.ratingService.getAllMissionRate(avatarMail).subscribe(data => {this.ratings = data; });
    console.log('counttttttttttttttt rtesSSSSSSSSSSSSSSS' + this.ratings.length);
  }
  // RateCount(avatarMail: string): number{
  //
  //
  //   // this.ratingService.getAllMissionRate(avatarMail).subscribe(data => {ratings = data; });
  //   this.ratingService.ratingsCount(avatarMail).subscribe(data => data);
  //
  // }

}
