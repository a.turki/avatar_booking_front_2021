import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvatardetailsComponent } from './avatardetails.component';

describe('AvatardetailsComponent', () => {
  let component: AvatardetailsComponent;
  let fixture: ComponentFixture<AvatardetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvatardetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatardetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
