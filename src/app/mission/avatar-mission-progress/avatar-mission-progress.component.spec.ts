import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvatarMissionProgressComponent } from './avatar-mission-progress.component';

describe('AvatarMissionProgressComponent', () => {
  let component: AvatarMissionProgressComponent;
  let fixture: ComponentFixture<AvatarMissionProgressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvatarMissionProgressComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarMissionProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
