import { Component, OnInit } from '@angular/core';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {Mission} from '../../models/mission';

@Component({
  selector: 'app-avatar-mission-progress',
  templateUrl: './avatar-mission-progress.component.html',
  styleUrls: ['./avatar-mission-progress.component.css']
})
export class AvatarMissionProgressComponent implements OnInit {
  isLoggedIn = false;
  doneMissions: Mission[];
  undoneMissions: Mission[];
  doingMissions: Mission[];
  AvatarEmail: string;
  constructor(private missionService: MissionService, private router: Router, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (!this.isLoggedIn){ this.router.navigate(['/home']); }

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.getUndoneMissions(user.email);
      this.getDoingMissions(user.email);
      this.getDoneMissions(user.email);
      this.AvatarEmail = user.email;
    }
  }
  getUndoneMissions(avatarMail: string): void {
    this.missionService.getUndoneMission(avatarMail).subscribe(data => {
      this.undoneMissions = data;
    });
  }
  getDoingMissions(avatarMail: string): void {
    this.missionService.getDoingMission(avatarMail).subscribe(data => {
      this.doingMissions = data;
    });
  }
  getDoneMissions(avatarMail: string): void {
    this.missionService.getDoneMission(avatarMail).subscribe(data => {
      this.doneMissions = data;
    });
  }

  VerifyIsAvatar(avatarMail: string): boolean {
    if (this.AvatarEmail === avatarMail)
    {
      console.log('is owner true');
      return true;
    }
  }
  isUndone(mission: Mission): boolean {
    if (mission.progress === 'undone' && mission.state === 'accepted')
    {
      return true;
    }

  }
  isDoing(mission: Mission): boolean {
    if (mission.progress === 'doing' && mission.state === 'accepted')
    {
      return true;
    }

  }

  StartMission(id: string, avatarMail: string): void {
    this.missionService.startMission(id).subscribe(data => {
      console.log(data);
      this.getUndoneMissions(avatarMail);
      this.getDoingMissions(avatarMail);
    }) ;
  }
  EndMission(id: string, avatarMail: string): void {
    this.missionService.endMission(id).subscribe(data => {
      console.log(data);
      this.getDoingMissions(avatarMail);
      this.getDoneMissions(avatarMail);
    }) ;
  }

}
