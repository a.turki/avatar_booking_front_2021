import {Component, Input, OnInit} from '@angular/core';
import {Mission} from '../../models/mission';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {MissionService} from '../../services/mission.service';
import {Offer} from '../../models/offer';
import {OfferService} from '../../services/offer.service';
import {Notification} from '../../models/notification';
import {NotificationService} from '../../services/notification.service';

@Component({
  selector: 'app-make-offer',
  templateUrl: './make-offer.component.html',
  styleUrls: ['./make-offer.component.css']
})
export class MakeOfferComponent implements OnInit {
  @Input() id: string;
  isLoggedIn = false;
  mission: Mission;
  offer = new Offer();

  mission2: Mission = new Mission();
  notification: Notification = new Notification();
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private offerService: OfferService, private route: ActivatedRoute, private token: TokenStorageService, private missionService: MissionService, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.token.getToken();
    // this.id = this.route.snapshot.params.id;
    if (this.isLoggedIn) {

      this.mission = new Mission();
      this.missionService.getMissionById(this.id).subscribe(data => {
        this.mission = data;
      });
      this.offer.missionId = this.id;
      this.offer.avatarMail = this.token.getUser().email;
    }
  }

  ApplyForMission(): void{

    // tslint:disable-next-line:max-line-length
    this.offerService.makeAnOffer(this.offer).subscribe(data => {console.log(data); }, error => console.log(error));
    this.missionService.getMissionById(this.id).subscribe(data => { this.mission2 = data;
      // tslint:disable-next-line:max-line-length
                                                                    this.notification.type = 'Applied for your mission : ' + data.title;
                                                                    this.notification.senderUsername = this.token.getUser().username;
                                                                    this.notification.missionID = this.mission2.id;
                                                                    this.notification.recipientUsername = this.mission2.clientemail;
      // tslint:disable-next-line:max-line-length
                                                                    this.notificationService.makeNotification(this.notification).subscribe(data1 => {console.log(data1); });
    });

    this.router.navigate(['/consultmission']).then(() => {
       window.location.reload();
     });


  }

}
