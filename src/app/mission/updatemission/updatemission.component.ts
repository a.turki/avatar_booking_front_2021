import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {MissionService} from '../../services/mission.service';
import {User} from '../../models/user';
import {Mission} from '../../models/mission';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-updatemission',
  templateUrl: './updatemission.component.html',
  styleUrls: ['./updatemission.component.css']
})
export class UpdatemissionComponent implements OnInit {

  id: string;
  mission: Mission;
  private missionListSubscription: Subscription;
 // @Input() id: string;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private route: ActivatedRoute, private token: TokenStorageService, private missionService: MissionService) { }

  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
    this.mission = new Mission();
    this.missionService.getMissionById(this.id).subscribe(data => {this.mission = data; });
    // console.log('******************' + this.id);
  }
  goToList(): void{
    this.router.navigate(['/allmissions']);
  }
  onSubmit(): void{
    console.log(this.mission);
    // this.employeeservice.updateEmployee(this.id,this.employee).subscribe(data => {
    //     this.goToEmployeeList();
    //   },error => console.log(error)
    // );

    this.missionService.updateMission(this.id, this.mission).subscribe(data => {
         this.goToList();
        }, error => console.log(error)
       );
    // tslint:disable-next-line:only-arrow-functions typedef

    // tslint:disable-next-line:only-arrow-functions typedef
   // setTimeout(function(){ window.location.reload(); }, 1);
    // tslint:disable-next-line:only-arrow-functions
    // this.goToList();
    // setTimeout(function(){ window.location.reload(); }, 1000);

  }

}
