import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvatarUndoneMissionComponent } from './avatar-undone-mission.component';

describe('AvatarUndoneMissionComponent', () => {
  let component: AvatarUndoneMissionComponent;
  let fixture: ComponentFixture<AvatarUndoneMissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvatarUndoneMissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarUndoneMissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
