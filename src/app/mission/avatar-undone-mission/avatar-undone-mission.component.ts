import { Component, OnInit } from '@angular/core';
import {Mission} from '../../models/mission';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';

@Component({
  selector: 'app-avatar-undone-mission',
  templateUrl: './avatar-undone-mission.component.html',
  styleUrls: ['./avatar-undone-mission.component.css']
})
export class AvatarUndoneMissionComponent implements OnInit {
  isLoggedIn = false;
  missions: Mission[];
  AvatarEmail: string;
  constructor(private missionService: MissionService, private router: Router, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.getUndoneMissions(user.email);
      this.AvatarEmail = user.email;
    }

    console.log('******************************' + this.AvatarEmail);
  }

  getUndoneMissions(avatarMail: string): void {
    this.missionService.getUndoneMission(avatarMail).subscribe(data => {
      this.missions = data;
    });
  }

   VerifyIsAvatar(avatarMail: string): boolean {
     if (this.AvatarEmail === avatarMail)
     {
       console.log('is owner true');
       return true;
     }
   }
   isUndone(mission: Mission): boolean {
    if (mission.progress === 'undone' && mission.state === 'accepted')
    {
      return true;
    }

   }

  StartMission(id: string, avatarMail: string): void {
    this.missionService.startMission(id).subscribe(data => {
      console.log(data);
      this.getUndoneMissions(avatarMail); }) ;
  }



}
