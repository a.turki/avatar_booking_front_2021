import { Component, OnInit } from '@angular/core';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {Mission} from '../../models/mission';
import {OfferService} from '../../services/offer.service';
import {Offer} from '../../models/offer';
import {MissionDetailsComponent} from '../../modals/mission-details/mission-details.component';
import {MatDialog} from '@angular/material/dialog';
import {MakeOfferModalComponent} from '../../modals/make-offer-modal/make-offer-modal.component';
import {Notification} from '../../models/notification';
import {NotificationService} from '../../services/notification.service';

@Component({
  selector: 'app-consultmission',
  templateUrl: './consultmission.component.html',
  styleUrls: ['./consultmission.component.css']
})
export class ConsultmissionComponent implements OnInit {
  // tslint:disable-next-line:max-line-length
  constructor(private missionService: MissionService, private offerService: OfferService , private router: Router, private tokenStorageService: TokenStorageService, private notificationService: NotificationService, public dialog1: MatDialog) { }
  isLoggedIn = false;
  missions: Mission[];
  mission2: Mission = new Mission();
  notification: Notification = new Notification();
  AvatarEmail: string;
  // pagination
  page = 1;
  count = 0;
  tableSize = 4;
  tableSizes = [ 4, 8, 12];

  dated = new Date('2021-06-26T00:00:00');

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (!this.isLoggedIn){ this.router.navigate(['/home']); }

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();

      this.getMissionsOpportunities(user.email);
      this.AvatarEmail = user.email;
      }

    console.log('******************************' + this.AvatarEmail);

}
// tslint:disable-next-line:typedef
  onTableDataChange(event){
    this.page = event;
    this.getMissionsOpportunities(this.AvatarEmail);
  }

  onTableSizeChange(event): void {
    this.tableSize = event.target.value;
    this.page = 1;
    this.getMissionsOpportunities(this.AvatarEmail);
  }

  getMissionsOpportunities(avatarMail: string): void {
    this.missionService.getMissionsListOpportunities(avatarMail).subscribe(data => {
      this.missions = data;
    });
  }

  AcceptMission(id: string, avatarMail: string): void{
    console.log('--------------------- id : ' + id);
    this.missionService.acceptMission(id, avatarMail).subscribe(data => {
      console.log(data);
      this.getMissionsOpportunities(avatarMail); }) ;
  }
  ApplyForMission(id: string): void{
    // tslint:disable-next-line:label-position
    const offer: Offer = new Offer();
    offer.missionId = id;
    this.missionService.getMissionById(id).subscribe(data => { this.mission2 = data; });
    offer.avatarMail = this.AvatarEmail;
    // offer.avatarUsername = this.tokenStorageService.getUser().username;
   // offer.avatarId = this.tokenStorageService.getUser().id;
    // tslint:disable-next-line:max-line-length
    this.offerService.makeAnOffer(offer).subscribe(data => {console.log(data);
                                                            this.getMissionsOpportunities(this.AvatarEmail);

    });

  }
  CancelOffer(id: string): void {
    // tslint:disable-next-line:max-line-length
    this.offerService.cancelAnOffer(id, this.AvatarEmail).subscribe(data => {console.log(data); this.getMissionsOpportunities(this.AvatarEmail); }, error => console.log(error));

  }

  RejectMission(id: string, avatarMail: string): void{
    this.missionService.declineMission(id, avatarMail).subscribe(data => {
      console.log(data);
      this.getMissionsOpportunities(avatarMail); }) ;
  }

  hasApplied(mission: Mission): boolean {

    if (mission.listavatarInterested.includes(this.AvatarEmail)){
      return true;
    }
  else {  return false; }
  }

  openDialogDetails(id: string): void{
    const dialogRef = this.dialog1.open(MissionDetailsComponent,
      {
        disableClose: true,
        height: '350px' ,
        width: '550px',
        data: { pageValue3: id }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }


  openDialogMakeOffer(id: string): void{
    const dialogRef = this.dialog1.open(MakeOfferModalComponent,
      {
        disableClose: true,
        data: { pageValue3: id }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }

  // tslint:disable-next-line:typedef
  triggerFunction() {
    console.log('Timer Ended');
  }

  dateToSecond(d: Date): number{
    console.log('-9*9-*9*-9*- ' + d );
    // const strdate = d.toISOString().slice(0, 16);
    // console.log('hour' + Date.parse(d.toUTCString()) / 1000 );
    const unixtimestamp =  (new Date(Date.now())).getTime() / 1000  ;

    return unixtimestamp;
  }
  dateToSecondB(d: Date): number{
    console.log('-9*9-*9*-9*- ' + d );
    // const strdate = d.toISOString().slice(0, 16);
    // console.log('hour' + Date.parse(d.toUTCString()) / 1000 );
    const unixtimestamp = (new Date(d)).getTime() / 1000;
   // const unixtimestampa =(new Date(d.toISOString().slice(0,16))).getTime() / 1000 ;

    return unixtimestamp;
  }

}
