import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultmissionComponent } from './consultmission.component';

describe('ConsultmissionComponent', () => {
  let component: ConsultmissionComponent;
  let fixture: ComponentFixture<ConsultmissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultmissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
