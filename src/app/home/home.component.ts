import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import {User} from '../models/user';
import {Router} from '@angular/router';
import {TokenStorageService} from '../services/token-storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  username: string;
  user: User;
  isLoggedIn = false;
  constructor(private userService: UserService, private router: Router, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    // this.userService.getUserByEmail('yassine.taktak@esprit.tn');
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    this.userService.getUserByEmail('yassine.taktak@esprit.tn').subscribe(data => { this.user = data; });
    this.username = this.user.username;
    console.log('************ ' + this.user.username);
   // if (this.isLoggedIn){ this.router.navigate(['/profile']); }
  }

}
