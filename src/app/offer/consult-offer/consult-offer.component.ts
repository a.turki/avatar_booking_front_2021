import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {MissionService} from '../../services/mission.service';
import {OfferService} from '../../services/offer.service';
import {Mission} from '../../models/mission';
import {User} from '../../models/user';
import {Offer} from '../../models/offer';
import {UserService} from '../../services/user.service';
import {MatDialog} from '@angular/material/dialog';
import {MyDialogModalComponent} from '../../modals/my-dialog-modal/my-dialog-modal.component';
import {AvatarRatesModalsComponent} from '../../modals/avatar-rates-modals/avatar-rates-modals.component';

@Component({
  selector: 'app-consult-offer',
  templateUrl: './consult-offer.component.html',
  styleUrls: ['./consult-offer.component.css']
})
export class ConsultOfferComponent implements OnInit {
  id: string;
  mission: Mission;
  offers: Offer[];
  isLoggedIn = false;

  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private route: ActivatedRoute, private token: TokenStorageService, private missionService: MissionService, private offerService: OfferService,
              private userService: UserService,
              public dialog1: MatDialog) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.token.getToken();
    if (!this.isLoggedIn){ this.router.navigate(['/home']); }
    this.id = this.route.snapshot.params.id;
    this.mission = new Mission();
    this.missionService.getMissionById(this.id).subscribe(data => {this.mission = data; });
    this.offerService.getMyMissionOffers(this.id).subscribe(data => {
      this.offers = data;

    });
  }

  getUsername(email: string): string{
    let username1: string;
    this.userService.getUserByEmail(email).subscribe(data => {username1 = data; });
    // return this.userService.getUserByEmail(email).
    return username1;
  }

  getMissionsOffers(id: string): void {
    // this.missionService.getMissionsListOpportunities(avatarMail).subscribe(data => {
    //   this.missions = data;
    // });
    this.offerService.getMyMissionOffers(this.id).subscribe(data => {
      this.offers = data;

    });
  }
  acceptOffer(ido: string): void{
    this.offerService.acceptAnOffer(ido).subscribe(data => {
      console.log(data);
      this.getMissionsOffers(this.id); });
  }

  declineOffer(ido: string): void{
    this.offerService.declineAnOffer(ido).subscribe(data => {
      console.log(data);
      this.getMissionsOffers(this.id); });
  }
  isPending(offer: Offer): boolean{

    return offer.state === 'pending';
  }
  openDialog(id: string): void {
    const dialogRef = this.dialog1.open(MyDialogModalComponent, {
      width: '400px',
      backdropClass: 'custom-dialog-backdrop-class',
      panelClass: 'custom-dialog-panel-class',
      data: { pageValue1: id }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      /*this.dialogValue = result.data;*/
    });
  }
  openDialogRates(avatarmail: string): void{
    const dialogRef = this.dialog1.open(AvatarRatesModalsComponent,
      {
        disableClose: true,
        height: '400px',
        data: { pageValue3: avatarmail }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }
}
