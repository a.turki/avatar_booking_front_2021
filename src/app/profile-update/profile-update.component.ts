import { Component, OnInit } from '@angular/core';
import {User} from '../models/user';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../services/user.service';
import {TokenStorageService} from '../services/token-storage.service';
import {DatePipe} from '@angular/common';


@Component({
  selector: 'app-profile-update',
  templateUrl: './profile-update.component.html',
  styleUrls: ['./profile-update.component.css']
})
export class ProfileUpdateComponent implements OnInit {
  user: User = new User();
  username1: string;
  currentUser: any;
  isLoggedIn = false;
  // tslint:disable-next-line:max-line-length
  constructor(private userservice: UserService , private router: Router, private route: ActivatedRoute, private token: TokenStorageService) { }

  ngOnInit(): void {
    this.currentUser = this.token.getUser();
    this.username1 = this.currentUser.username;
    this.userservice.getEmployeeByUsername(this.username1).subscribe(data => {this.user = data; } , error => console.log(error));
    this.user.username = this.username1;
    console.log('dato ' + this.user.dateofBirth);


  }
  // tslint:disable-next-line:typedef
  goToProfile(){
    this.router.navigate(['/profile']);

  }
  // tslint:disable-next-line:typedef
  onSubmit(){
    console.log(this.user);
    /*this.saveEmployee();*/
    this.userservice.updateProfile(this.username1, this.user).subscribe(data => {
        this.goToProfile();
        window.location.reload();
      }, error => console.log(error)
    );

  }

}
