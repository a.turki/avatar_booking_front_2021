import { Component, OnInit } from '@angular/core';
import {Reclamation} from '../../models/reclamation';
import {ReclamationService} from '../../services/reclamation.service';
import {ToastService} from '../../services/toast.service';
import {MatDialog} from '@angular/material/dialog';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {MyDeleteModalsComponent} from '../../modals/my-delete-modals/my-delete-modals.component';
import {DeleteClaimModalComponent} from '../../modals/delete-claim-modal/delete-claim-modal.component';

@Component({
  selector: 'app-historique-reclamation',
  templateUrl: './historique-reclamation.component.html',
  styleUrls: ['./historique-reclamation.component.css']
})
export class HistoriqueReclamationComponent implements OnInit {
  private roles: string[];
  showAdminBoard = false;
  showModeratorBoard = false;
  showAvatarBoard = false;
  showClientBoard = false;
  isLoggedIn = false;
  // **************
  reclamations: Reclamation[];
  // tslint:disable-next-line:max-line-length
  constructor(private reclamationService: ReclamationService, private toastService: ToastService, public dialog1: MatDialog, private missionservice: MissionService, private router: Router, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (!this.isLoggedIn){ this.router.navigate(['/home']); }
    if (this.isLoggedIn) {

      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showAvatarBoard = this.roles.includes('ROLE_AVATAR');
      this.showClientBoard = this.roles.includes('ROLE_CLIENT');

      if (this.showClientBoard){
        this.getReclamations();
      }
    }

  }

  treatClaim(reclamation: Reclamation): void {
    this.reclamationService.treatReclamation(reclamation).subscribe(data => {console.log(data);
                                                                             this.getReclamations(); });

  }

  getReclamations(): void{
    this.reclamationService.getReclamationByEmail(this.tokenStorageService.getUser().email).subscribe(data => {
      this.reclamations = data;
    });

  }

  openDialogD(id: string): void{
    const dialogRef = this.dialog1.open(DeleteClaimModalComponent,
      {
        disableClose: true,
        width: '400px',
        data: { pageValue2: id }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });
    // dialogRef.afterClosed().subscribe(()=>);

    // ...
  }


}
