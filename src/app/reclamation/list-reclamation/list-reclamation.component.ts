import { Component, OnInit } from '@angular/core';
import {ReclamationService} from '../../services/reclamation.service';
import {ToastService} from '../../services/toast.service';
import {MatDialog} from '@angular/material/dialog';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {Reclamation} from '../../models/reclamation';

@Component({
  selector: 'app-list-reclamation',
  templateUrl: './list-reclamation.component.html',
  styleUrls: ['./list-reclamation.component.css']
})
export class ListReclamationComponent implements OnInit {
  private roles: string[];
  showAdminBoard = false;
  showModeratorBoard = false;
  showAvatarBoard = false;
  showClientBoard = false;
  isLoggedIn = false;
  // **************
  reclamations: Reclamation[];
  // tslint:disable-next-line:max-line-length
  constructor(private reclamationService: ReclamationService, private toastService: ToastService, public dialog1: MatDialog, private missionservice: MissionService, private router: Router, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (!this.isLoggedIn){ this.router.navigate(['/home']); }
    if (this.isLoggedIn) {

      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showAvatarBoard = this.roles.includes('ROLE_AVATAR');
      this.showClientBoard = this.roles.includes('ROLE_CLIENT');

      if (this.showAdminBoard){
      this.getReclamations();
    }
    }

  }

  treatClaim(reclamation: Reclamation): void {
   this.reclamationService.treatReclamation(reclamation).subscribe(data => {console.log(data);
                                                                            this.getReclamations(); });

  }

  getReclamations(): void{
    this.reclamationService.getReclamationsList().subscribe(data => {
      this.reclamations = data;
    });
}


}
