import {Component, Input, OnInit} from '@angular/core';
import {Mission} from '../../models/mission';
import {ActivatedRoute, Router} from '@angular/router';
import {MissionService} from '../../services/mission.service';
import {Reclamation} from '../../models/reclamation';
import {ReclamationService} from '../../services/reclamation.service';
import {ToastService} from '../../services/toast.service';

@Component({
  selector: 'app-delete-reclamation',
  templateUrl: './delete-reclamation.component.html',
  styleUrls: ['./delete-reclamation.component.css']
})
export class DeleteReclamationComponent implements OnInit {
  @Input() id: string;
  reclamation: Reclamation;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private route: ActivatedRoute, private reclamationService: ReclamationService, private toastService: ToastService) { }

  ngOnInit(): void {
    this.reclamation = new Reclamation();
    this.reclamationService.getReclamationById(this.id).subscribe(data => {this.reclamation = data; });



  }
  deleteReclamation( id: string): void {
    this.reclamationService.deleteReclamationId(id).subscribe(data => {
      console.log(data);
    });


    this.goToClaimList();
    window.location.reload();

  }
  cancelDelete(): void
  {
    this.goToClaimList();
    window.location.reload();
  }
  // tslint:disable-next-line:typedef
  showToasterSuccess(){

    this.toastService.showSuccess('Claim deleted successfully !!', '');

  }
  goToClaimList(): void{
    this.router.navigate(['/myclaimhistory']);

  }

}
