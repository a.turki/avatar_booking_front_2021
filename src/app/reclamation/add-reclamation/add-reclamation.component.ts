import {Component, Input, OnInit} from '@angular/core';
import {ReclamationService} from '../../services/reclamation.service';
import {AdminService} from '../../services/admin.service';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {Mission} from '../../models/mission';
import {Reclamation} from '../../models/reclamation';
import {ToastService} from '../../services/toast.service';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-add-reclamation',
  templateUrl: './add-reclamation.component.html',
  styleUrls: ['./add-reclamation.component.css']
})
export class AddReclamationComponent implements OnInit {
  @Input() missiondid: string;
  mission: Mission = new Mission();

  reclamation: Reclamation = new Reclamation();
  reclamation1: Reclamation = new Reclamation();

  // tslint:disable-next-line:max-line-length
  constructor(private reclamationService: ReclamationService, private toastService: ToastService, public dialog1: MatDialog, private missionservice: MissionService, private router: Router, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {

   this.reclamation.claimerName = this.tokenStorageService.getUser().email;
   this.reclamation.missionID = this.missiondid;

  }
  saveReclamation(): void{
    this.reclamationService.createReclamation(this.reclamation).subscribe(data => {this.reclamation1 = data; });
  }
  goToMissionList(): void{
    this.router.navigate(['/allmissions']);

  }
  onSubmit(): void{
    console.log(this.mission);
    /*this.saveEmployee();*/
    this.saveReclamation();
    this.goToMissionList();
    this.dialog1.closeAll();
    this.showToasterSuccess();

    // this.dialogRef.close();
    // this.dialogRef.afterClosed().subscribe(result => { this.showToasterSuccess(); });


    // console.log(this.missionid);
    // tslint:disable-next-line:only-arrow-functions typedef
    setTimeout(function(){ window.location.reload(); }, 1000);


  }
  // tslint:disable-next-line:typedef
  showToasterSuccess(){

    this.toastService.showSuccess('Claim added successfully !!', this.mission.title);

  }


}
