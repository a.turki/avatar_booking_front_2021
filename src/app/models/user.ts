import {Photo} from './photo';

export class User {
  id: string;
  username: string;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  dateofBirth: Date;
  sexe: string;
  address: string;
  tel: string;

  city: string;
  country: string;

  facebook: string;
  website: string;
  git: string;
  aboutMe: string;
  field: string;
  studyLevel: string;
  languages: string[];
  profession: string;

  rating: number;
  ratingcount: number;



image: Photo;
constructor() {}
}
