import {Offer} from './offer';
import {User} from './user';
import {Reclamation} from './reclamation';

export class Mission {
  id: string;
  title: string;
  description: string;
  price: number;
  address: string;
  country: string;
  region: string;
  date: Date;
  expiredate: Date;
  time: string;
  duration: number;
  preference: string;
  clientemail: string;
  type: string;
  listavatarCandidat: string[];
  listavatarDeclined: string[];
  listavatarInterested: string[];
  avataremail: string;
  dateStr: string;
  done: string;
  rating: number;
  feedback: string;
  state: string;
  progress: string;
  rated: boolean;
 // listavatarOffer: Offer[];
  client: User;
  avatar: User;
  reclamations: Reclamation[];
  offers: Offer[];
  claimed: string[];
  constructor() {
  }
}
