export class Message {
  id: string;
  senderId: string;
  receiverId: string;
  content: string;
  sentAt: Date;
  seen: boolean;

  // constructor(senderId: string, receiverId: string, content: string){
  //   this.senderId = senderId;
  //   this.receiverId = receiverId;
  //   this.content = content;
  // }
  constructor() {
  }
}
