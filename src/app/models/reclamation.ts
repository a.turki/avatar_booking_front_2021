import {Mission} from './mission';
import {User} from './user';

export class Reclamation {

  id: string;
  title: string;
  type: string;
  description: string;
  processed: boolean;
  missionID: string;
  mission: Mission ;
  decision: string;
  claimerName: string;
  claimer: User;



}
