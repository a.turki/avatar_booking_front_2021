import {User} from './user';

export class Offer {
  id: string;
  missionId: string;
  avatarMail: string;
  // avatarId: string;
  avatar: User;
  price: number;
  state: string;
  accepeted: boolean;
  date: Date;
  constructor() {
  }
}
