export class Rating {
  id: string;
  missionId: string;
  clientEmail: string;
  avatarEmail: string;
  rate: number;
  feedback: string;
}
