export class Notification {

  id: string;
  senderUsername: string;
  recipientUsername: string;
  type: string;
  missionID: string;
  seen: boolean;
  sentAt: Date;
  seenAt: Date;

}
