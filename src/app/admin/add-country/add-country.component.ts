import { Component, OnInit } from '@angular/core';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {AdminService} from '../../services/admin.service';
import {Mission} from '../../models/mission';
import {Country} from '../../models/country';

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.css']
})
export class AddCountryComponent implements OnInit {
  country: Country = new Country();
  Countries: Country[];
  isLoggedIn = false;
  constructor(private adminservice: AdminService, private router: Router, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (!this.isLoggedIn){ this.router.navigate(['/home']); }

    this.getCountries();
  }


  getCountries(): void {
    this.adminservice.getCountryList().subscribe(data => {
      this.Countries = data;
    });
  }
  saveCountry(): void{
    this.adminservice.createCountry(this.country).subscribe(data => {console.log(data);
                                                                     this.getCountries(); });
  }

}
