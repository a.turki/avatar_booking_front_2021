import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../../services/token-storage.service';
import {MissionService} from '../../services/mission.service';
import {Region} from '../../models/region';

@Component({
  selector: 'app-charts1',
  templateUrl: './charts1.component.html',
  styleUrls: ['./charts1.component.css']
})
export class Charts1Component implements OnInit {
  title = 'Missions By Progress';
  type = 'PieChart';
  stat = [];
  ListProgress: string[];
  data = [
    ['Firefox', 45.0],
    ['IE', 26.8],
    ['Chrome', 12.8],
    ['Safari', 8.5],
    ['Opera', 6.2],
    ['Others', 0.7]
  ];

  columnNames = ['Browser', 'Percentage'];
  options = {
    colors: ['#0e2ae0', '#3e4fe6', '#6e83ec', '#9fadf3', '#aac8fc'], is3D: false
  };
  width = 550;
  height = 400;
  constructor(private tokenStorageService: TokenStorageService, private missionService: MissionService) { }

  ngOnInit(): void {

    this.missionService.getListProgress().subscribe(data => {this.ListProgress = data;
                                                             console.log(this.ListProgress);
                                                             for (const val of this.ListProgress){
        this.loadDataChart(val);
        console.log('****************** t: ' + val);
      }
                                                             console.log(this.stat); } );


    // for (const val of this.ListProgress){
    //   this.loadDataChart(val);
    //   console.log('****************** t: ' + val);
    // }


  }

  loadDataChart(progress: string): void{
    const t = [];
    let i: number;
    t.push(progress);
    this.missionService.getCountProgress(progress).subscribe(data => { i = data;
                                                                       t.push(i);
                                                                       console.log('****************** i: ' + i);
                                                                       console.log('tab ' + t);
                                                                       this.stat.push(t); });



  }



}
