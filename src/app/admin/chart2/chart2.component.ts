import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../../services/token-storage.service';
import {MissionService} from '../../services/mission.service';

@Component({
  selector: 'app-chart2',
  templateUrl: './chart2.component.html',
  styleUrls: ['./chart2.component.css']
})
export class Chart2Component implements OnInit {
  title = 'Number of Mission by Country';
  type = 'PieChart';
  stat = [];
  ListCountry: string[];
  data = [
    ['Firefox', 45.0],
    ['IE', 26.8],
    ['Chrome', 12.8],
    ['Safari', 8.5],
    ['Opera', 6.2],
    ['Others', 0.7]
  ];

  columnNames = ['Country', 'Number'];
  options = {
    colors: ['#2c48f1', '#3e4fe6', '#6e83ec', '#9fadf3', '#aac8fc'], is3D: false, pieHole: 0.4
  };
  width = 550;
  height = 400;
  constructor(private tokenStorageService: TokenStorageService, private missionService: MissionService) { }

  ngOnInit(): void {
    this.missionService.getListCountryOfMissions().subscribe(data => {this.ListCountry = data;
                                                                      console.log(this.ListCountry);
                                                                      for (const val of this.ListCountry){
        this.loadDataChart(val);
        console.log('****************** t: ' + val);
      }
                                                                      console.log(this.stat); } );

  }
  loadDataChart(country: string): void{
    const t = [];
    let i: number;
    t.push(country);
    this.missionService.getCountCountry(country).subscribe(data => { i = data;
                                                                     t.push(i);
                                                                     console.log('****************** i: ' + i);
                                                                     console.log('tab ' + t);
                                                                     this.stat.push(t); });



  }
}
