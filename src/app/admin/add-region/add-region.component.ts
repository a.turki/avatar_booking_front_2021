import { Component, OnInit } from '@angular/core';
import {Country} from '../../models/country';
import {AdminService} from '../../services/admin.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {Region} from '../../models/region';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-add-region',
  templateUrl: './add-region.component.html',
  styleUrls: ['./add-region.component.css']
})
export class AddRegionComponent implements OnInit {
  country: Country = new Country();
  region: Region = new Region();
  Countries: Country[];
  Regions: Region[];
  isLoggedIn = false;
  constructor(private adminservice: AdminService, private router: Router, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (!this.isLoggedIn){ this.router.navigate(['/home']); }
    this.getCountries();
    this.getRegions();
  }


  getCountries(): void {
    this.adminservice.getCountryList().subscribe(data => {
      this.Countries = data;
    });
  }
  getRegions(): void {
    this.adminservice.getRegionList().subscribe(data => {
      this.Regions = data;
    });
  }
  saveRegion(saveForm: NgForm): void{
    this.adminservice.createRegion(this.region).subscribe(data => {console.log(data);
                                                                   this.getRegions();
                                                                    });
    saveForm.reset();


  }

}
