import {Component, Input, OnInit} from '@angular/core';
import {Mission} from '../../models/mission';
import {MissionService} from '../../services/mission.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {TokenStorageService} from '../../services/token-storage.service';
import {MyDeleteModalsComponent} from '../../modals/my-delete-modals/my-delete-modals.component';
import {MissionDetailsComponent} from '../../modals/mission-details/mission-details.component';

@Component({
  selector: 'app-reprofile',
  templateUrl: './reprofile.component.html',
  styleUrls: ['./reprofile.component.css']
})
export class ReprofileComponent implements OnInit {
  missions: Mission[];
  // mod
  dialogValue1: string;
  sendValue1: string;
  // **
  isLoggedIn = false;
  ClientEmail: string;
  // pagination
  page = 1;
  count = 0;
  tableSize = 4;
  tableSizes = [ 4, 8, 12];
  ida = '608aaefdecc4c926a37598ff';
  // tslint:disable-next-line:max-line-length
  constructor(private missionService: MissionService, private router: Router, public dialog1: MatDialog, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.getMyMissions(user.email);
      this.ClientEmail = user.email;
    }

    console.log('******************************' + this.ClientEmail);
  }

  getMyMissions(clientMail: string): void {
    this.missionService.getMyMissionsList(clientMail).subscribe(data => {
      this.missions = data;
    });
  }

  deleteMission( id: string): void {
    this.missionService.deleteMissionParId(id).subscribe(data => {
      console.log(data);
      this.getMyMissions(this.ClientEmail); });

  }
  openDialogD(id: string): void{
    const dialogRef = this.dialog1.open(MyDeleteModalsComponent,
      {
        disableClose: true,
        width: '400px',
        data: { pageValue2: id }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }
  openDialogDetails(id: string): void{
    const dialogRef = this.dialog1.open(MissionDetailsComponent,
      {
        disableClose: true,
        width: '50%',
        data: { pageValue3: id }

      });
    /*
       Subscribe to events emitted when the backdrop is clicked
       NOTE: Since we won't actually be using the `MouseEvent` event, we'll just use an underscore here
       See https://stackoverflow.com/a/41086381 for more info
    */
    dialogRef.backdropClick().subscribe(() => {
      // Close the dialog
      dialogRef.close();
    });

    // ...
  }
  isPending(mission: Mission): boolean {

    if (mission.state === 'pending'){
      return true;
    }

  }
  isPendingProgress(mission: Mission): boolean {

    if (mission.progress === 'pending'){
      return true;
    }

  }
  hide(mission: Mission): boolean{
    if (this.isPending(mission) && this.isPendingProgress(mission))
    {
      return true;
    }
  }
}
