import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReprofileComponent } from './reprofile.component';

describe('ReprofileComponent', () => {
  let component: ReprofileComponent;
  let fixture: ComponentFixture<ReprofileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReprofileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
