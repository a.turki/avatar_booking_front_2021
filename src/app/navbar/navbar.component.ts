import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../services/token-storage.service';
import {MessageService} from '../services/message.service';
import {Message} from '../models/message';
import {MissionService} from '../services/mission.service';
import {NotificationService} from '../services/notification.service';
import {Notification} from '../models/notification';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  private roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username: string;
  email: string;
  listdiscussion: string[];
  chatMessages: Message[] = [];
  rer: string;
  lastMsg: Message;
  Messages: Message[] = [];
  lastMessages: Message[] = [];
  notifications: Notification[];
  messageCount = 0;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private tokenStorageService: TokenStorageService, private notificationService: NotificationService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');

      this.username = user.username;
      this.email = user.email;
    //  console.log('99999999' + this.username);
      this.getMyLastMessages(this.username);


      this.getMyNotifications();

     // console.log( this.lastMessages);

    }
  }
  getMyLastMessages(user1: string): void {
    // tslint:disable-next-line:label-position
    // let msg: Message;
  //  this.messageService.getDiscussion(this.username, user2).subscribe(data => {this.Messages = data; });
  // this.lastMsg = this.Messages[this.Messages.length - 1];
    // msg = this.Messages[this.Messages.length - 1];
   // return this.lastMsg.content;
    this.messageService.getMyLastMessages(user1).subscribe(data => {this.lastMessages = data; });


  }
  checkNotification(): void{

    for (const n of this.notifications) {
     // console.log(n);
      if (!n.seen){
        this.notificationService.checkNotification(n.id).subscribe(data => {
          this.getMyNotifications();
         // console.log('seen');
          });
      }

    }
  }

  getMyNotifications(): void {
    this.notificationService.getMyUncheckedNotifications(this.email).subscribe(data => {
      this.notifications = data;
    });
  }
  isSender(msg: Message): boolean{
    if (msg.senderId === this.username)
    {
      return true;
    }
    return false;
  }

  logout(): void {
    this.tokenStorageService.signOut();
    this.router.navigate(['/home']);
    // window.localStorage.removeItem('auth-token');
    // window.localStorage.clear();
    // this.goToHome();
    window.location.reload();
  }
  goToHome(): void{
    this.router.navigate(['']);

  }

  countUnseen(): number{
    this.messageCount = 0;
    for (const n of this.lastMessages) {
      // console.log(n);
      if ((n.seen && !this.isSender(n) ) || this.isSender(n)) {
        this.messageCount = this.messageCount + 1;
      }

    }

    return this.messageCount;
  }

}
