import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {BoardAdminComponent} from './board-admin/board-admin.component';
import {BoardUserComponent} from './board-user/board-user.component';
import {BoardModeratorComponent} from './board-moderator/board-moderator.component';
import {ProfileComponent} from './profile/profile.component';
import {ProfileUpdateComponent} from './profile-update/profile-update.component';
import {PhotoComponent} from './photo/photo.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {RecoverPasswordComponent} from './recover-password/recover-password.component';
import {ReprofileComponent} from './try/reprofile/reprofile.component';
import {ListmissionComponent} from './mission/listmission/listmission.component';
import {UpdatemissionComponent} from './mission/updatemission/updatemission.component';
import {AddmissionComponent} from './mission/addmission/addmission.component';
import {MissiondetailsComponent} from './mission/missiondetails/missiondetails.component';
import {AvatardetailsComponent} from './mission/avatardetails/avatardetails.component';
import {ConsultmissionComponent} from './mission/consultmission/consultmission.component';
import {AvatarUndoneMissionComponent} from './mission/avatar-undone-mission/avatar-undone-mission.component';
import {MymissionsComponent} from './mission/mymissions/mymissions.component';
import {MissionAvatarsComponent} from './mission/mission-avatars/mission-avatars.component';
import {AvatarDoingMissionComponent} from './mission/avatar-doing-mission/avatar-doing-mission.component';
import {AvatarDoneMissionComponent} from './mission/avatar-done-mission/avatar-done-mission.component';
import {RateMissionComponent} from './mission/rate-mission/rate-mission.component';
import {AvatarAllRatingComponent} from './mission/avatar-all-rating/avatar-all-rating.component';
import {AddSpecificMissionSelectAvatarsComponent} from './mission/add-specific-mission-select-avatars/add-specific-mission-select-avatars.component';
import {ChatComponent} from './chat/chat/chat.component';
import {DiscussionComponent} from './chat/discussion/discussion.component';
import {MissionDetails2Component} from './mission/mission-details2/mission-details2.component';
import {ConsultOfferComponent} from './offer/consult-offer/consult-offer.component';
import {NotificationComponent} from './notification/notification.component';
import {HomeComponent} from './home/home.component';
import {AvatarMissionProgressComponent} from './mission/avatar-mission-progress/avatar-mission-progress.component';
import {ChatRoomComponent} from './chat/chat-room/chat-room.component';
import {ChatDiscussionComponent} from './chat/chat-discussion/chat-discussion.component';
import {ProfileVisitComponent} from './profile-visit/profile-visit.component';
import {MakeOfferComponent} from './mission/make-offer/make-offer.component';
import {AddCountryComponent} from './admin/add-country/add-country.component';
import {AddRegionComponent} from './admin/add-region/add-region.component';
import {Charts1Component} from './admin/charts1/charts1.component';
import {Chart2Component} from './admin/chart2/chart2.component';
import {AddReclamationComponent} from './reclamation/add-reclamation/add-reclamation.component';
import {ListReclamationComponent} from './reclamation/list-reclamation/list-reclamation.component';
import {Addmission2Component} from './mission/addmission2/addmission2.component';
import {DashboardChartsComponent} from './admin/dashboard-charts/dashboard-charts.component';
import {PageComponent} from './HomePage/page/page.component';
import {HistoriqueReclamationComponent} from './reclamation/historique-reclamation/historique-reclamation.component';


const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'user', component: BoardUserComponent },
  { path: 'mod', component: BoardModeratorComponent },
  { path: 'admin', component: BoardAdminComponent },
  { path: 'profileupdate', component: ProfileUpdateComponent },
  { path: 'photo', component: PhotoComponent },
  { path: 'resetpassword', component: ResetPasswordComponent },
  { path: 'recovpassword/:email', component: RecoverPasswordComponent },
  { path: 'tryprof', component: ReprofileComponent},
  { path: 'allmissions', component: ListmissionComponent},
  { path: 'createmission', component: AddmissionComponent},
  { path: 'updatemission/:id', component: UpdatemissionComponent},
  { path: 'detailsmission/:id', component: MissiondetailsComponent},
  { path: 'missiondetails/:id', component: MissionDetails2Component},
  { path: 'avatarinformation', component: AvatardetailsComponent},
  { path: 'consultmission', component: ConsultmissionComponent},
  { path: 'undonemission', component: AvatarUndoneMissionComponent},
  { path: 'doingmission', component: AvatarDoingMissionComponent},
  { path: 'donemission', component: AvatarDoneMissionComponent},
  { path: 'mymission', component: MymissionsComponent},
  { path: 'missionavatars', component: MissionAvatarsComponent},
  { path: 'ratemission', component: RateMissionComponent},
  { path: 'rating', component: AvatarAllRatingComponent},
  { path: 'specificmission', component: AddSpecificMissionSelectAvatarsComponent},
  { path: 'chat/:receiver', component: ChatComponent},
  { path: 'discussion/:receiver', component: DiscussionComponent},
  { path: 'mymissionoffers/:id', component: ConsultOfferComponent},
  { path: 'notification', component: NotificationComponent},
  { path: 'avatarmissionboard', component: AvatarMissionProgressComponent},
  { path: 'chatroom/:id', component: ChatRoomComponent},
  { path: 'chatroom', component: ChatRoomComponent},
  { path: 'chatdiscu/:id', component: ChatDiscussionComponent},
  { path: 'visitprofile/:id', component: ProfileVisitComponent},
  { path: 'makeoffer/:id', component: MakeOfferComponent},
  { path: 'addcountry', component: AddCountryComponent},
  { path: 'chart', component: Charts1Component},
  { path: 'myclaimhistory', component: HistoriqueReclamationComponent},
  { path: 'chart2', component: Chart2Component},
  { path: 'DashboardChart', component: DashboardChartsComponent},
  { path: 'addregion', component: AddRegionComponent},
  { path: 'addreclamation', component: AddReclamationComponent},
  { path: 'Listreclamation', component: ListReclamationComponent},
  { path: 'add2', component: Addmission2Component},
  { path: 'home', component: HomeComponent},
  { path: 'page', component: PageComponent},
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { initialNavigation: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
