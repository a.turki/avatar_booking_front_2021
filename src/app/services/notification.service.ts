import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from './token-storage.service';
import {Notification} from '../models/notification';
import {Observable} from 'rxjs';
import {Mission} from '../models/mission';
const API_URL = 'http://localhost:8080/api/';
@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private http: HttpClient, private token: TokenStorageService) { }

  makeNotification(notification: Notification): Observable<any> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.post(API_URL + 'notify', notification, {headers: header});
  }

  getMyNotifications(recipientUsername: string): Observable<Notification[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Notification[]>(API_URL + 'allMyNotifications/' + recipientUsername, {headers: header});
  }

  getMyUncheckedNotifications(recipientUsername: string): Observable<Notification[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Notification[]>(API_URL + 'MyUnseenNotifications/' + recipientUsername, {headers: header});
  }

  checkNotification(id: string): Observable<any>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.put(API_URL + 'CheckNotification/' + id, {headers: header});
  }



}
