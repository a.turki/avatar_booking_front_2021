import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from './token-storage.service';
import {Mission} from '../models/mission';
import {Observable} from 'rxjs';
import {Country} from '../models/country';
import {Region} from '../models/region';

const API_URL = 'http://localhost:8080/api/';
@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient, private token: TokenStorageService) { }

  createCountry(country: Country): Observable<any> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.post(API_URL + 'addcountry', country, {headers: header});
  }

  createRegion(region: Region): Observable<any> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.post(API_URL + 'addregion', region, {headers: header});
  }

  getCountryList(): Observable<Country[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Country[]>(API_URL + 'allCountry', {headers: header});
  }

  getRegionList(): Observable<Region[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Region[]>(API_URL + 'allRegions', {headers: header});
  }

  getCountryRegionList(country: string): Observable<Region[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Region[]>(API_URL + 'allCountryRegions/' + country , {headers: header});
  }


}
