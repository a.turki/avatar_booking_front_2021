import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from './token-storage.service';
import {Observable} from 'rxjs';
import {Mission} from '../models/mission';
import {Photo} from '../models/photo';

const API_URL = 'http://localhost:8080/api/';
@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  constructor(private http: HttpClient, private token: TokenStorageService) { }

  getPhotoByUsername(imageName: string): Observable<any> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<any>(API_URL + 'get/' + imageName, {headers: header});
  }
}
