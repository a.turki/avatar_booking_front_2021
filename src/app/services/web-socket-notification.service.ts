import { Injectable } from '@angular/core';
import {Notification} from '../models/notification';
import {MessageService} from './message.service';
import {ChatMessage} from '../models/chat-message';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class WebSocketNotificationService {
  webSocket: WebSocket;
  chatMessages: Notification[] = [];
  notifications: Notification[] = [];

  constructor(private messageService: MessageService, private notificationService: NotificationService) { }

  // tslint:disable-next-line:typedef
  public openWebSocket(username1: string){
    this.webSocket = new WebSocket('ws://localhost:8080/notification');

    this.webSocket.onopen = (event) => {
      console.log('Open: ', event);
    };

    this.notificationService.getMyNotifications(username1).subscribe(data => {this.notifications = data; });
    this.webSocket.onmessage = (event) => {
      const notification = JSON.parse(event.data);
      this.notifications.push(notification);
    };

    this.webSocket.onclose = (event) => {
      console.log('Close: ', event);
    };
  }

  // tslint:disable-next-line:typedef
  public sendNotification(notification: Notification){
    this.webSocket.send(JSON.stringify(notification));
  }

  // tslint:disable-next-line:typedef
  public closeWebSocket() {
    this.webSocket.close();
  }

}
