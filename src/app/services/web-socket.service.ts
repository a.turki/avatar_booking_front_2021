import { Injectable } from '@angular/core';
import {Message} from '../models/message';
import {ChatMessage} from '../models/chat-message';
import {MessageService} from './message.service';
import {TokenStorageService} from './token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  webSocket: WebSocket;
  chatMessages: Message[] = [];

  constructor(private messageService: MessageService, private token: TokenStorageService) { }
  // tslint:disable-next-line:typedef
  public openWebSocket(username1: string, username2: string){
    this.webSocket = new WebSocket('ws://localhost:8080/chat');

    this.webSocket.onopen = (event) => {
      console.log('Open: ', event);
    };

    this.messageService.getDiscussion(username1, username2)
                .subscribe(data => {this.chatMessages = data;
                                    this.checkMessage(username1, username2); });


    // this.checkMessage(username1, username2);
    this.webSocket.onmessage = (event) => {
      const chatMessage = JSON.parse(event.data);
      console.log('message ' + chatMessage);
      this.chatMessages.push(chatMessage);
      this.messageService.getDiscussion(username1, username2).subscribe(data => {this.chatMessages = data; });
      // window.location.reload();
    };

    this.webSocket.onclose = (event) => {
      console.log('Close: ', event);
    };
  }
  checkMessage(username1: string, username2: string): void{

    for (const n of this.chatMessages) {

      if ((!n.seen) && (n.receiverId === this.token.getUser().username)){
        // this.notificationService.checkNotification(n.id).subscribe(data => {
        //   this.getMyNotifications();
        //   console.log('seen'); });
        this.messageService.checkMessage(n.id, n).subscribe(data => { this.getMyDiscussion(username1, username2); });
      }

    }
  }
  // tslint:disable-next-line:typedef
  public sendMessage(chatMessage: ChatMessage){
    this.webSocket.send(JSON.stringify(chatMessage));
  }

  // tslint:disable-next-line:typedef
  public closeWebSocket() {
    this.webSocket.close();
  }

  getMyDiscussion(username1: string, username2: string): void{
    this.messageService.getDiscussion(username1, username2).subscribe(data => {this.chatMessages = data; });

  }

}
