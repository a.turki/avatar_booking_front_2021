import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {User} from '../models/user';
import {TokenStorageService} from './token-storage.service';
import { map } from 'rxjs/operators';
import {Mission} from '../models/mission';


const API_URL = 'http://localhost:8080/api/test/';
const API_URL1 = 'http://localhost:8080/api/auth/user/';
const API_URL2 = 'http://localhost:8080/api/profile/';




@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseURL = 'http://localhost:8080/api/profile/user';
  private baseURLO = 'http://localhost:8080/api/profile/userprofile';
  // private API_URL1 = 'http://localhost:8080/api/auth/user/';
  // head = this.token.getToken();
  // private head: HttpHeaders;
  constructor( private http: HttpClient, private token: TokenStorageService) {

  }


  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }

  getUserBoard(): Observable<any> {
    return this.http.get(API_URL + 'user', { responseType: 'text' });
  }

  getModeratorBoard(): Observable<any> {
    return this.http.get(API_URL + 'mod', { responseType: 'text' });
  }

  getAdminBoard(): Observable<any> {
    return this.http.get(API_URL + 'admin', { responseType: 'text' });
  }

  updateProfile(username: string, user: User): Observable<object> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.put(`${this.baseURLO}/${username}`, user, {headers: header});
  }

  // getEmployeeByUsername(username: string): Observable<User> {
  //   return this.http.get<User>(`${this.baseURL}/${username}`);
  // }
  getEmployeeByUsername(username: string): Observable<any> {
        const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
        return this.http.get<User>(`${this.baseURL}/${username}`, {headers: header});
    }

  getUserById(id: string): Observable<any> {
    // const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<User>(API_URL1 + id);
  }
  getUserByEmail(email: string): Observable<any> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<User>(API_URL2 + 'userByEmail/' + email, {headers: header});
  }


}
