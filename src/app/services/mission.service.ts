import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from './token-storage.service';
import {Mission} from '../models/mission';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import {User} from '../models/user';



const API_URL = 'http://localhost:8080/api/';
const API_URL1 = 'http://localhost:8080/api/auth/';

@Injectable({
  providedIn: 'root'
})
export class MissionService {

  constructor(private http: HttpClient, private token: TokenStorageService) { }


  createMission(mission: Mission): Observable<any> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.post(API_URL + 'addmission', mission, {headers: header});
  }
  createMission2(mission: Mission): Observable<Mission> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.post<Mission>(API_URL + 'addmission2', mission, {headers: header});
  }
  getMissionsList(): Observable<Mission[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Mission[]>(API_URL + 'allMissions', {headers: header});
  }
  getMyMissionsList(clientMail: string): Observable<Mission[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Mission[]>(API_URL + 'allMyMissions/' + clientMail, {headers: header});
  }
   deleteMissionParId(id): Observable<any> {
     const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
     return this.http
      .delete(API_URL + 'deleteMissionById/' + id, {headers: header})
      .pipe(map((resp) => resp));
  }
  // public getMissionById(id): Observable<any> {
  //   const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
  //
  //   return this.http
  //     .get<Mission>(API_URL + 'findMissionById/' + id, {headers: header});
  // }
  getMissionById(id: string): Observable<Mission> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Mission>(API_URL + 'findMissionById/' + id, {headers: header});
  }

  updateMission(id: string, mission: Mission): Observable<object> {
    return this.http.put(API_URL + 'updateMission/' + id, mission);
  }

  getAvatarsList(): Observable<User[]> {
    // const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<User[]>(API_URL1 + 'allAvatar');
  }
  getMissionAvatarsList(id: string): Observable<User[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<User[]>(API_URL + 'allMissionAvatars/' + id, {headers: header});
  }
  getAvatarsListStr(): Observable<string[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<string[]>(API_URL + 'allAvatarStr', {headers: header});
  }

  getAvatarsListStrByRegion(region: string): Observable<string[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<string[]>(API_URL + 'allAvatarStr/' + region, {headers: header});
  }

  getAvatarsListUsernameStr(): Observable<string[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<string[]>(API_URL + 'allAvatarUsernameStr', {headers: header});
  }
  getMissionsListOpportunities(avatarMail: string): Observable<Mission[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Mission[]>(API_URL + 'MissionOpportunities/' + avatarMail, {headers: header});
  }

  declineMission(id: string, avatarMail: string): Observable<object> {
    return this.http.get(API_URL + 'declineMission/' + id + '/' + avatarMail);
  }

  acceptMission(id: string, avatarMail: string): Observable<object> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get(API_URL + 'acceptMission/' + id + '/' + avatarMail, {headers: header});
  }

  getUndoneMission(avatarMail: string): Observable<Mission[]>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Mission[]>(API_URL + 'myundonemission/' + avatarMail , {headers: header});
  }

  startMission(id: string): Observable<object>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get(API_URL + 'startMission/' + id , {headers: header});

  }
  endMission(id: string): Observable<object>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get(API_URL + 'endMission/' + id , {headers: header});

  }

  getDoingMission(avatarMail: string): Observable<Mission[]>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Mission[]>(API_URL + 'mydoingmission/' + avatarMail , {headers: header});
  }

  getDoneMission(avatarMail: string): Observable<Mission[]>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Mission[]>(API_URL + 'mydonemission/' + avatarMail , {headers: header});
  }

  rated(missionid: string): Observable<any>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.get(API_URL + 'ratedmission/' + missionid, {headers: header});
  }

  getFinishedMission(avatarMail: string): Observable<Mission[]>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Mission[]>(API_URL + 'avatarfinishedmission/' + avatarMail , {headers: header});
  }

  getListProgress(): Observable<string[]>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.get<string[]>(API_URL + 'distinctProgress' , {headers: header});


  }
  getListCountryOfMissions(): Observable<string[]>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.get<string[]>(API_URL + 'distinctCountry' , {headers: header});


  }

  getCountProgress(progress: string ): Observable<number>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<number>(API_URL + 'countProgress/' + progress , {headers: header});
  }
  getCountCountry(country: string ): Observable<number>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<number>(API_URL + 'countCountry/' + country , {headers: header});
  }


}
