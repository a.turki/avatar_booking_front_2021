import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from './token-storage.service';
import {Mission} from '../models/mission';
import {Observable} from 'rxjs';
import {Reclamation} from '../models/reclamation';
import {map} from 'rxjs/operators';

const API_URL = 'http://localhost:8080/api/';

@Injectable({
  providedIn: 'root'
})
export class ReclamationService {

  constructor(private http: HttpClient, private token: TokenStorageService) { }

  createReclamation(reclamation: Reclamation): Observable<Reclamation> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.post<Reclamation>(API_URL + 'addreclamation', reclamation, {headers: header});
  }

  getReclamationsList(): Observable<Reclamation[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Reclamation[]>(API_URL + 'allReclamation', {headers: header});
  }
  treatReclamation(reclamation: Reclamation): Observable<any> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.post(API_URL + 'TreatClaim', reclamation, {headers: header});

  }

  getReclamationByEmail(email: string): Observable<Reclamation[]>
  {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Reclamation[]>(API_URL + 'allReclamationClaimer/' + email, {headers: header});
  }
  deleteReclamationId(id): Observable<any> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http
      .delete(API_URL + 'deleteReclamation/' + id, {headers: header})
      .pipe(map((resp) => resp));
  }
   verifreclamationexist(missionid: string, claimerMail: string): Observable<boolean>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<boolean>(API_URL + 'verifreclamationexist/' + missionid + '/' + claimerMail, {headers: header});
  }

  getReclamationById(id: string): Observable<Reclamation> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Reclamation>(API_URL + 'getreclamation/' + id, {headers: header});
  }


}
