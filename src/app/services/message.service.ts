import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from './token-storage.service';
import {Mission} from '../models/mission';
import {Observable} from 'rxjs';
import {Message} from '../models/message';
import {map} from 'rxjs/operators';
const API_URL = 'http://localhost:8080/api/';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient, private token: TokenStorageService) { }

  // createMission(mission: Mission): Observable<any> {
  //   const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
  //
  //   return this.http.post(API_URL + 'addmission', mission, {headers: header});
  // }

  sendMessage(message: Message): Observable<any>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.post(API_URL + 'sendMessage', message, {headers: header});

  }

  getDiscussion(user1: string, user2: string): Observable<Message[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Message[]>(API_URL + 'discussion/' + user1 + '/' + user2, {headers: header});
  }

 /* getListDiscussion(user1: string ): Observable<string[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<string[]>(API_URL + 'listdiscussion/' + user1, {headers: header});
  }

  getLastMessage(user1: string, user2: string): Observable<Message> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Message>(API_URL + 'lastmessage/' + user1 + '/' + user2, {headers: header});
  }*/

  getMyLastMessages(user1: string): Observable<Message[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Message[]>(API_URL + 'getMyLastMessages/' + user1 , {headers: header});
  }

  checkMessage(id: string , message: Message): Observable<object>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return  this.http.put(API_URL + 'CheckMessage/' + id, message, {headers: header});

  }

  countUnseen(message: Message ): Observable<number>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<number>(API_URL + 'countunseen/' + message.receiverId + '/' + message.senderId, {headers: header});
  }

}
