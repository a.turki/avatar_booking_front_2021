import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from './token-storage.service';
import {Mission} from '../models/mission';
import {Observable} from 'rxjs';
import {Rating} from '../models/rating';

const API_URL = 'http://localhost:8080/api/';

@Injectable({
  providedIn: 'root'
})
export class RatingService {

  constructor(private http: HttpClient, private token: TokenStorageService) { }

  rateMission(rating: Rating, missionid: string): Observable<any> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.post(API_URL + 'ratemission/' + missionid, rating, {headers: header});
  }

  getAllMissionRate(avatarMail: string): Observable<Rating[]>{
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Rating[]>(API_URL + 'AvatarRates/' + avatarMail , {headers: header});
  }

  ratingsCount(avatarMail: string): Observable<number>
  {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<number>(API_URL + 'AvatarRatesCount/' + avatarMail , {headers: header});

  }




}
