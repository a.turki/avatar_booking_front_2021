import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from './token-storage.service';
import {Mission} from '../models/mission';
import {Observable} from 'rxjs';
import {Offer} from '../models/offer';

const API_URL = 'http://localhost:8080/api/';
@Injectable({
  providedIn: 'root'
})
export class OfferService {

  constructor(private http: HttpClient, private token: TokenStorageService) { }
  makeAnOffer(offer: Offer): Observable<any> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.post(API_URL + 'makeanoffer', offer, {headers: header});
  }
  cancelAnOffer(id: string, avatarmail: string): Observable<any> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.post(API_URL + 'cancelanoffer/' + id + '/' + avatarmail, {headers: header});
  }
  acceptAnOffer(id: string): Observable<object> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.post(API_URL + 'acceptoffer/' + id, {headers: header});
  }
  declineAnOffer(id: string): Observable<object> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });

    return this.http.post(API_URL + 'declineoffer/' + id, {headers: header});
  }
  getMyMissionOffers(id: string): Observable<Offer[]> {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    return this.http.get<Offer[]>(API_URL + 'OffersByMission/' + id, {headers: header});
  }

}

