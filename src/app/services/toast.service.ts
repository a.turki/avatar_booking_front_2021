import { Injectable } from '@angular/core';
import {GlobalConfig, ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  public optionsToaster: GlobalConfig;
  constructor(private toastr: ToastrService) { }
  // tslint:disable-next-line:typedef
  showSuccess(message, title){

    this.toastr.success(message, title);
    this.optionsToaster = this.toastr.toastrConfig;
    this.optionsToaster.timeOut = 100000;
    this.optionsToaster.autoDismiss = true;
    this.optionsToaster.positionClass = 'toast-bottom-right';

  }



  // tslint:disable-next-line:typedef
  showError(message, title){

    this.toastr.error(message, title);

  }



  // tslint:disable-next-line:typedef
  showInfo(message, title){

    this.toastr.info(message, title);

  }



  // tslint:disable-next-line:typedef
  showWarning(message, title){

    this.toastr.warning(message, title);

  }
}
