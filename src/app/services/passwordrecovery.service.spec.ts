import { TestBed } from '@angular/core/testing';

import { PasswordrecoveryService } from './passwordrecovery.service';

describe('PasswordrecoveryService', () => {
  let service: PasswordrecoveryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PasswordrecoveryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
