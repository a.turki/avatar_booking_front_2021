import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';


const AUTH_APII = 'http://localhost:8080/api/auth/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PasswordrecoveryService {

  constructor(private http: HttpClient) { }

  recoverPasswordDemand(emaill: string): Observable<any>{
        console.log('recover demand enter');
        return this.http.post('http://localhost:8080/tak/test/resetpwd', {
          email: emaill
        });
  }

  recoverTokenCreation(emaill: string): Observable<any>{
    console.log('recover token creation enter');

    return this.http.get('http://localhost:8080/tak/test/resettoken?email=' + emaill);
  }

  newPwd(user): Observable<any> {
    return this.http.post('http://localhost:8080/api/auth/newpassword', {
      email: user.email,
      password: user.password
    }, httpOptions);
  }


}
