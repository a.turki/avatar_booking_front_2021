import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../services/token-storage.service';
import {UserService} from '../services/user.service';
import {User} from '../models/user';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Country} from '../models/country';
import {Region} from '../models/region';
import {AdminService} from '../services/admin.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  private roles: string[];
  showAdminBoard = false;
  showModeratorBoard = false;
  showAvatarBoard = false;
  showClientBoard = false;
  isLoggedIn = false;
  // **************
  currentUser: any;
  user: User = new User();
  idU: string;
  updateduser: User = new User();
  username1: string;

  editOn: boolean;
  test ?: any;
  rates: any;
  // tslint:disable-next-line:max-line-length
  // ************photo
  selectedFile: File;
  imgURL: any;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;

  Countries: Country[];
  Regions: Region[];
  // tslint:disable-next-line:max-line-length
  constructor(private adminservice: AdminService, private httpClient: HttpClient, private token: TokenStorageService, private userservice: UserService, private router: Router, private route: ActivatedRoute) { }
  getImage(): void {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    this.httpClient.get('http://localhost:8080/api/get/' + this.imageName, { headers: header})
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.token.getToken();
    if (!this.isLoggedIn){ this.router.navigate(['/home']); }
    this.getCountries();

    if (this.isLoggedIn) {

      const user = this.token.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      this.showAvatarBoard = this.roles.includes('ROLE_AVATAR');
      this.showClientBoard = this.roles.includes('ROLE_CLIENT');


      this.editOn = false;
      this.currentUser = this.token.getUser();
      this.username1 = this.currentUser.username;
      this.rates = 2.6666667;
      // tslint:disable-next-line:max-line-length
      this.userservice.getEmployeeByUsername(this.username1).subscribe(data => {
        this.user = data;
        this.idU = this.user.id;

      }, error => console.log(error));
      this.userservice.getEmployeeByUsername(this.username1).subscribe(data => {

        this.updateduser = data;

      }, error => console.log(error));
      this.user.username = this.username1;
      // *******************************************
      this.imageName = this.token.getUser().username;
      console.log('photo');
      console.log('username:' + this.imageName);
      this.getImage();

    }
    else if (!this.isLoggedIn)
    {
      this.router.navigate(['/home']);
    }

  }
  getUser(): void{
  this.userservice.getEmployeeByUsername(this.username1).subscribe(data => {
  this.user = data;

} , error => console.log(error));
}

  edit(): void{
    this.editOn = true;
    this.adminservice.getCountryRegionList(this.updateduser.country).subscribe(data =>
    { this.Regions = data ; });
  }

  isUserCity(city: string): boolean{

    return this.updateduser.city === city;
  }

  cancel(): void{
    this.editOn = false;
  }

  gotoFB(): void{
    // window.location.href = this.user.facebook;
    window.open(this.user.facebook);
  }
  gotoWebsite(): void{
    // window.location.href = this.user.facebook;
    window.open(this.user.website);
  }
  gotoGit(): void{
    window.open(this.user.git);
  }

  // tslint:disable-next-line:typedef
  onSubmit(){
    console.log(this.user);
    /*this.saveEmployee();*/
    this.userservice.updateProfile(this.username1, this.updateduser).subscribe(data => {
        // this.updateduser = data;
       // this.user = this.updateduser;
        this.editOn = false;
        this.getUser();
        // window.location.reload();
      }, error => console.log(error)
    );

  }

  getCountries(): void {
    this.adminservice.getCountryList().subscribe(data => {
      this.Countries = data;
    });
  }
  // tslint:disable-next-line:typedef
  onChange($event: any) {
    this.adminservice.getCountryRegionList(this.updateduser.country).subscribe(data =>
    { this.Regions = data ; });
  }

}
