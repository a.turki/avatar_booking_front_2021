import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from '../services/token-storage.service';
import {base64ToFile, ImageCroppedEvent} from 'ngx-image-cropper';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnInit {
  private fileToReturn: File;

  constructor(private httpClient: HttpClient, private token: TokenStorageService) { }
  selectedFile: File;
  imageFile: File;
  image: any;
  imgURL: any;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
  selected = false;

  username: string;

  imageChangedEvent: any = '';
  croppedImage: any = '';


  // tslint:disable-next-line:typedef
  public onFileChanged(event) {
    this.imageChangedEvent = event;
    // Select File
    this.selectedFile = event.target.files[0];
    this.selected = true;
    // Below part is used to display the selected image
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (event2) => {
      this.imgURL = reader.result;
    };
  }


  // tslint:disable-next-line:typedef
  onUpload() {
    console.log(this.selectedFile);
    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.fileToReturn, this.fileToReturn.name);

    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    this.httpClient.post('http://localhost:8080/api/upload/' + this.imageName, uploadImageData, { observe: 'response' , headers: header})
      .subscribe((response) => {
          if (response.status === 200) {
            this.message = 'Image uploaded successfully';
          } else {
            this.message = 'Image not uploaded successfully';
          }
        }
      );
    // tslint:disable-next-line:only-arrow-functions typedef
    setTimeout(function(){ window.location.reload(); }, 1500);


  }

  // tslint:disable-next-line:typedef
  getImage() {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    this.httpClient.get('http://localhost:8080/api/get/' + this.imageName, { headers: header})
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
  }

  ngOnInit(): void {
    this.username = this.token.getUser().username;
    this.imageName = this.token.getUser().username;
    console.log('username:' + this.imageName);
    this.getImage();
  }




  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  // tslint:disable-next-line:typedef
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    // this.selectedFile = event.

    this.fileToReturn = this.base64ToFile(
      event.base64,
      this.imageChangedEvent.target.files[0].name,
    );

    console.log(this.imageChangedEvent.target.files[0]);
    console.log(this.fileToReturn);
    // return this.fileToReturn;

    // ****
    // this.image = base64ToFile(this.croppedImage);
    // this.imageFile = new File([this.imageFile], this.username);

  }
  // tslint:disable-next-line:typedef
  imageLoaded() {
    /* show cropper */
  }
  // tslint:disable-next-line:typedef
  cropperReady() {
    /* cropper ready */
  }
  // tslint:disable-next-line:typedef
  loadImageFailed() {
    /* show message */
  }

  // tslint:disable-next-line:typedef
  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/png' });
    return blob;
  }

  // tslint:disable-next-line:typedef
  base64ToFile(data, filename) {

    const arr = data.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while (n--){
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, { type: mime });
  }

}
