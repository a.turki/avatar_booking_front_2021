import {Component, Inject, OnDestroy, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-my-dialog-modal',
  templateUrl: './my-dialog-modal.component.html',
  styleUrls: ['./my-dialog-modal.component.css']
})
export class MyDialogModalComponent implements OnInit,OnDestroy {
  fromPage: string;
  fromDialog: string;

  constructor(
    public dialogRef: MatDialogRef<MyDialogModalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.fromPage = data.pageValue1;
    console.log('--------------------------------------------- ' + this.fromPage);
  }

  ngOnInit(): void{
  }

  closeDialog(): void{
    this.dialogRef.close({ event: 'close', data: this.fromDialog });
  }

  ngOnDestroy(): void {
  }

  }
