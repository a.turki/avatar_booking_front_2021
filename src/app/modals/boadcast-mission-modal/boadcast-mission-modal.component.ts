import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-boadcast-mission-modal',
  templateUrl: './boadcast-mission-modal.component.html',
  styleUrls: ['./boadcast-mission-modal.component.css']
})
export class BoadcastMissionModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<BoadcastMissionModalComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }
  closeDialog(): void{
    this.dialogRef.close();
  }

}
