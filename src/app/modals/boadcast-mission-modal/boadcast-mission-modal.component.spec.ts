import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoadcastMissionModalComponent } from './boadcast-mission-modal.component';

describe('BoadcastMissionModalComponent', () => {
  let component: BoadcastMissionModalComponent;
  let fixture: ComponentFixture<BoadcastMissionModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoadcastMissionModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoadcastMissionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
