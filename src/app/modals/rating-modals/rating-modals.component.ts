import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-rating-modals',
  templateUrl: './rating-modals.component.html',
  styleUrls: ['./rating-modals.component.css']
})
export class RatingModalsComponent implements OnInit {
  fromPage: string;
  fromDialog: string;
  constructor(public dialogRef: MatDialogRef<RatingModalsComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.fromPage = data.pageValue3;
    console.log('--------------------------------------------- ' + this.fromPage);
  }

  ngOnInit(): void {
  }
  closeDialog(): void{
    this.dialogRef.close({ event: 'close', data: this.fromDialog });
  }
}
