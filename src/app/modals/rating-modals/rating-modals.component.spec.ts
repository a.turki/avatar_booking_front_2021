import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingModalsComponent } from './rating-modals.component';

describe('RatingModalsComponent', () => {
  let component: RatingModalsComponent;
  let fixture: ComponentFixture<RatingModalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RatingModalsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingModalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
