import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-specific-mission-modal',
  templateUrl: './specific-mission-modal.component.html',
  styleUrls: ['./specific-mission-modal.component.css']
})
export class SpecificMissionModalComponent implements OnInit {
  fromPage: string[];
  fromDialog: string;
  constructor(public dialogRef: MatDialogRef<SpecificMissionModalComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.fromPage = data.pageValue3;
    console.log('--------------------------------------------- ' + this.fromPage);
  }

  ngOnInit(): void {
  }
  closeDialog(): void{
    this.dialogRef.close({ event: 'close', data: this.fromDialog });
  }

}
