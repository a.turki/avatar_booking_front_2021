import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificMissionModalComponent } from './specific-mission-modal.component';

describe('SpecificMissionModalComponent', () => {
  let component: SpecificMissionModalComponent;
  let fixture: ComponentFixture<SpecificMissionModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecificMissionModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificMissionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
