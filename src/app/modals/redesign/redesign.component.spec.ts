import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedesignComponent } from './redesign.component';

describe('RedesignComponent', () => {
  let component: RedesignComponent;
  let fixture: ComponentFixture<RedesignComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedesignComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RedesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
