import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeOfferModalComponent } from './make-offer-modal.component';

describe('MakeOfferModalComponent', () => {
  let component: MakeOfferModalComponent;
  let fixture: ComponentFixture<MakeOfferModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MakeOfferModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeOfferModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
