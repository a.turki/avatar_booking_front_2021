import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MakeOfferComponent} from '../../mission/make-offer/make-offer.component';

@Component({
  selector: 'app-make-offer-modal',
  templateUrl: './make-offer-modal.component.html',
  styleUrls: ['./make-offer-modal.component.css']
})
export class MakeOfferModalComponent implements OnInit {
  fromPage: string;
  fromDialog: string;

  constructor(
    public dialogRef: MatDialogRef<MakeOfferComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.fromPage = data.pageValue3;
    console.log('--------------------------------------------- ' + this.fromPage);
  }

  ngOnInit(): void{
  }

  closeDialog(): void{
    this.dialogRef.close({ event: 'close', data: this.fromDialog });
  }

}
