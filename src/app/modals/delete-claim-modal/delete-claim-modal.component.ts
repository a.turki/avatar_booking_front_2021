import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-delete-claim-modal',
  templateUrl: './delete-claim-modal.component.html',
  styleUrls: ['./delete-claim-modal.component.css']
})
export class DeleteClaimModalComponent implements OnInit {
  fromPage1: string;
  fromDialog1: string;
  claimId: string;

  constructor(
    public dialogRef1: MatDialogRef<DeleteClaimModalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef1.disableClose = true;
    this.fromPage1 = data.pageValue2;
    console.log('--------------------------------------------- ' + this.fromPage1);
    this.claimId = this.fromPage1;
    console.log('||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||' + this.claimId);
    console.log('this is claimid' + this.claimId);
  }

  ngOnInit(): void{
  }

  closeDialog(): void{
    this.dialogRef1.close({ event: 'close', data: this.fromDialog1 });
  }

}
