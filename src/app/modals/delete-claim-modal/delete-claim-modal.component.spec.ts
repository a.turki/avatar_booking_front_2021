import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteClaimModalComponent } from './delete-claim-modal.component';

describe('DeleteClaimModalComponent', () => {
  let component: DeleteClaimModalComponent;
  let fixture: ComponentFixture<DeleteClaimModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteClaimModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteClaimModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
