import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReclamationModalsComponent } from './reclamation-modals.component';

describe('ReclamationModalsComponent', () => {
  let component: ReclamationModalsComponent;
  let fixture: ComponentFixture<ReclamationModalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReclamationModalsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReclamationModalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
