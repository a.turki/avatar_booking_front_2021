import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-avatar-rates-modals',
  templateUrl: './avatar-rates-modals.component.html',
  styleUrls: ['./avatar-rates-modals.component.css']
})
export class AvatarRatesModalsComponent implements OnInit {
  fromPage: string;
  fromDialog: string;
  constructor(public dialogRef: MatDialogRef<AvatarRatesModalsComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.fromPage = data.pageValue3;
    console.log('--------------------------------------------- ' + this.fromPage);
  }

  ngOnInit(): void {
  }
  closeDialog(): void{
    this.dialogRef.close({ event: 'close', data: this.fromDialog });
  }

}
