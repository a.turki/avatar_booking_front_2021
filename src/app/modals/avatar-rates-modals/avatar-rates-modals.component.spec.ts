import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvatarRatesModalsComponent } from './avatar-rates-modals.component';

describe('AvatarRatesModalsComponent', () => {
  let component: AvatarRatesModalsComponent;
  let fixture: ComponentFixture<AvatarRatesModalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvatarRatesModalsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarRatesModalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
