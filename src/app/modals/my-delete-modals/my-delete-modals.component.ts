import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-my-delete-modals',
  templateUrl: './my-delete-modals.component.html',
  styleUrls: ['./my-delete-modals.component.css']
})
export class MyDeleteModalsComponent implements OnInit {
  fromPage1: string;
  fromDialog1: string;
  missionId: string;

  constructor(
    public dialogRef1: MatDialogRef<MyDeleteModalsComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef1.disableClose = true;
    this.fromPage1 = data.pageValue2;
    console.log('--------------------------------------------- ' + this.fromPage1);
    this.missionId = this.fromPage1;
    console.log('||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||' + this.missionId);
    console.log('this is missionID' + this.missionId);
  }

  ngOnInit(): void{
    }

  closeDialog(): void{
    this.dialogRef1.close({ event: 'close', data: this.fromDialog1 });
  }

}
