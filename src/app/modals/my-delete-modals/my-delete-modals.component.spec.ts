import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyDeleteModalsComponent } from './my-delete-modals.component';

describe('MyDeleteModalsComponent', () => {
  let component: MyDeleteModalsComponent;
  let fixture: ComponentFixture<MyDeleteModalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyDeleteModalsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyDeleteModalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
