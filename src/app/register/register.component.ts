import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [AuthService]
})
export class RegisterComponent implements OnInit {
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  rolesSelected: string[] = [];
  asAvatar = false;

  constructor(private route: Router, private authService: AuthService) { }

  ngOnInit(): void {
  }
  tologin(): void{
    this.route.navigate(['/login']);
 }
  registerasAvatar(): void{

    this.rolesSelected.push('avatar');
    this.asAvatar = true;
    console.log(this.rolesSelected);
  }
  registerasAvatarDeselect(): void{

    this.rolesSelected.splice(this.rolesSelected.indexOf('avatar'), 1);

    this.asAvatar = false;
    console.log(this.rolesSelected);
  }
  VerifyAsAvatarSelected(): boolean {
    if (this.rolesSelected.includes('avatar'))
    {
      return true;
    }
    else { return false; }

  }

  onSubmit(): void {
    this.form.roles = this.rolesSelected;
    this.authService.register(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }



}
