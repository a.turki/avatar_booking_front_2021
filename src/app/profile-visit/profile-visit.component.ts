import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from '../services/token-storage.service';
import {UserService} from '../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../models/user';
import {Rating} from '../models/rating';
import {MissionService} from '../services/mission.service';
import {RatingService} from '../services/rating.service';

@Component({
  selector: 'app-profile-visit',
  templateUrl: './profile-visit.component.html',
  styleUrls: ['./profile-visit.component.css']
})
export class ProfileVisitComponent implements OnInit {
  private roles: string[];
  ratings: Rating[];
  showAdminBoard = false;
  showModeratorBoard = false;
  showAvatarBoard = false;
  showClientBoard = false;
  // **************
  currentUser: any;
  user: User = new User();
  idU: string;
  updateduser: User = new User();
  username1: string;
  username2: string;
  isLoggedIn = false;
  editOn: boolean;
  test ?: any;
  rates: any;
  // tslint:disable-next-line:max-line-length
  // ************photo
  selectedFile: File;
  imgURL: any;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
  // tslint:disable-next-line:max-line-length
  constructor(private ratingService: RatingService , private httpClient: HttpClient, private token: TokenStorageService, private userservice: UserService, private router: Router, private route: ActivatedRoute) { }
  getImage(): void {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.token.getToken()}` });
    this.httpClient.get('http://localhost:8080/api/get/' + this.imageName, { headers: header})
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.token.getToken();
    if (!this.isLoggedIn){ this.router.navigate(['/home']); }
    if (this.isLoggedIn) {

     // const user = this.token.getUser();
     // this.roles = user.roles;



      this.username2 = this.route.snapshot.params.id;
      this.editOn = false;
      this.currentUser = this.token.getUser();
      this.username1 = this.currentUser.username;
      this.rates = 2.6666667;
      // tslint:disable-next-line:max-line-length
      this.userservice.getEmployeeByUsername(this.username2).subscribe(data => {
        this.user = data;
        this.showAvatarBoard = true;
        this.ratingService.getAllMissionRate(this.user.email).subscribe(data1 => {this.ratings = data1; });


      }, error => console.log(error));
      this.userservice.getEmployeeByUsername(this.username2).subscribe(data => {

        this.updateduser = data;

      }, error => console.log(error));
      this.user.username = this.username1;
      // *******************************************
      this.imageName = this.username2;
      console.log('photo');
      console.log('username:' + this.imageName);
      this.getImage();

    }
    else if (!this.isLoggedIn)
    {
      this.router.navigate(['/home']);
    }

  }
  getUser(): void{
    this.userservice.getEmployeeByUsername(this.username1).subscribe(data => {
      this.user = data;

    } , error => console.log(error));
  }

  edit(): void{
    this.editOn = true;
  }

  cancel(): void{
    this.editOn = false;
  }

  gotoFB(): void{
    // window.location.href = this.user.facebook;
    window.open(this.user.facebook);
  }
  gotoWebsite(): void{
    // window.location.href = this.user.facebook;
    window.open(this.user.website);
  }
  gotoGit(): void{
    window.open(this.user.git);
  }

  // tslint:disable-next-line:typedef
  onSubmit(){
    console.log(this.user);
    /*this.saveEmployee();*/
    this.userservice.updateProfile(this.username1, this.updateduser).subscribe(data => {
        // this.updateduser = data;
        // this.user = this.updateduser;
        this.editOn = false;
        this.getUser();
        // window.location.reload();
      }, error => console.log(error)
    );

  }


}
