import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PasswordrecoveryService} from '../services/passwordrecovery.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  email: string;
  success = false;
  failed = false;
  errorMessage = '';
  constructor(private route: ActivatedRoute, private passwordRecover: PasswordrecoveryService) { }

  ngOnInit(): void {
    // this.email = this.route.snapshot.params.email;
  //  console.log('*******' + this.email);

  }

  onSubmit(): void{
    console.log(this.email + 'recupered mail from input');
    // tslint:disable-next-line:max-line-length
    this.passwordRecover.recoverPasswordDemand(this.email).subscribe(data => {console.log(data);
                                                                              this.success = true; },
        error => {
          this.success = false;
          this.failed = true;
          this.errorMessage = error.error.message;

        });
    console.log('clicked');

  }

}
