import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {WebSocketService} from '../../services/web-socket.service';
import {MessageService} from '../../services/message.service';
import {TokenStorageService} from '../../services/token-storage.service';
import {ActivatedRoute} from '@angular/router';
import {Message} from '../../models/message';
import {NgForm} from '@angular/forms';
import {ChatMessage} from '../../models/chat-message';

@Component({
  selector: 'app-chat-discussion',
  templateUrl: './chat-discussion.component.html',
  styleUrls: ['./chat-discussion.component.css']
})
export class ChatDiscussionComponent implements OnInit , OnDestroy{
  sender: string;
  receiver: string;
  loggedUser: string;
  message: Message = new Message();

  // ************photo loged
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  // *** Receiver
  retrievedImageReceiver: any;
  base64DataReceiver: any;
  retrieveResonseReceiver: any;

  imageName: any;
  // tslint:disable-next-line:max-line-length
  constructor(private httpClient: HttpClient, public webSocketService: WebSocketService, private messageService: MessageService, private tokenStorageService: TokenStorageService, private route: ActivatedRoute) { }
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  ngOnInit(): void {
    this.receiver = this.route.snapshot.params.id;
    this.loggedUser = this.tokenStorageService.getUser().username;
    this.sender = this.tokenStorageService.getUser().username;

    this.getImageLoggedUser();
    this.getImageReceiver();
    // this.receiver = this.route.snapshot.params.receiver;
    this.webSocketService.openWebSocket(this.sender, this.receiver);
  }

  // tslint:disable-next-line:typedef use-lifecycle-interface
  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

  ngOnDestroy(): void {
    this.webSocketService.closeWebSocket();
  }
  // tslint:disable-next-line:typedef
  sendMessage(sendForm: NgForm) {
    // const chatMessageDto = new ChatMessage(sendForm.value.user, sendForm.value.message);
    const chatMessageDto = new ChatMessage(this.sender, sendForm.value.message);
    this.message.senderId = this.sender;
    // this.message.receiverId = 'testtt receiver' ;
    this.message.receiverId = this.receiver;
    this.message.content = sendForm.value.message;

    this.messageService.sendMessage(this.message).subscribe(data => {console.log(data); }, error => console.log(error));

    this.webSocketService.sendMessage(chatMessageDto);
    // window.location.reload();
    sendForm.controls.message.reset();
  }

  isSender(username: string): boolean
  {
    if (username === this.loggedUser){
      return true;
    }
    else { return false; }

  }

  // tslint:disable-next-line:typedef
  getImageLoggedUser() {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.tokenStorageService.getToken()}` });
    this.httpClient.get('http://localhost:8080/api/get/' + this.loggedUser, { headers: header})
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
  }
  // tslint:disable-next-line:typedef
  getImageReceiver() {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.tokenStorageService.getToken()}` });
    this.httpClient.get('http://localhost:8080/api/get/' + this.receiver, { headers: header})
      .subscribe(
        res => {
          this.retrieveResonseReceiver = res;
          this.base64DataReceiver = this.retrieveResonseReceiver.picByte;
          this.retrievedImageReceiver = 'data:image/jpeg;base64,' + this.base64DataReceiver;
        }
      );
  }
}
