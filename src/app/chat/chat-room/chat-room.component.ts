import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TokenStorageService} from '../../services/token-storage.service';
import {NotificationService} from '../../services/notification.service';
import {MessageService} from '../../services/message.service';
import {Message} from '../../models/message';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {WebSocketService} from '../../services/web-socket.service';
import {NgForm} from '@angular/forms';
import {ChatMessage} from '../../models/chat-message';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.css']
})
export class ChatRoomComponent implements OnInit, OnDestroy {
  isLoggedIn = false;
  username: string;
  selectedDiscussion: string;
  lastMessages: Message[] = [];
  // fromm chat discussion

  sender: string;
  receiver: string;
  loggedUser: string;
  message: Message = new Message();

  // ************photo loged
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  // *** Receiver
  retrievedImageReceiver: any;
  base64DataReceiver: any;
  retrieveResonseReceiver: any;

  imageName: any;


  // tslint:disable-next-line:max-line-length
  constructor(private httpClient: HttpClient, public webSocketService: WebSocketService, private route: ActivatedRoute, private router: Router, private tokenStorageService: TokenStorageService, private notificationService: NotificationService, private messageService: MessageService) {
    this.route.paramMap.subscribe(params => {
      this.ngOnInit();
    });
  }
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  ngOnInit(): void {
   // this.id = this.route.snapshot.params.id;

    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (!this.isLoggedIn){ this.router.navigate(['/home']); }
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();

      this.username = user.username;
      console.log('99999999' + this.username);
      this.getMyLastMessages(this.username);
      console.log( this.lastMessages);
      // from on init chat discussion
      this.receiver = this.route.snapshot.params.id;
      this.loggedUser = this.tokenStorageService.getUser().username;
      this.sender = this.tokenStorageService.getUser().username;

      this.getImageLoggedUser();
      this.getImageReceiver();
      // this.receiver = this.route.snapshot.params.receiver;
      this.webSocketService.openWebSocket(this.sender, this.receiver);
      // from on init chat discussion

    }
    else if (!this.isLoggedIn)
    {
      this.router.navigate(['/home']);
      // window.location.reload();
    }
  }
  isSender2(username: string): boolean
  {
    if (username === this.loggedUser){
      return true;
    }
    else { return false; }

  }

  getMyLastMessages(user1: string): void {

    this.messageService.getMyLastMessages(user1).subscribe(data => {this.lastMessages = data; });

  }

  isSender(msg: Message): boolean{
    if (msg.senderId === this.username)
    {
      return true;
    }
    return false;
  }

  // tslint:disable-next-line:typedef
  openDiscussion(m: Message){
    if ( this.isSender(m))
    {
      this.receiver = m.receiverId;
    }
    else { this.receiver = m.senderId; }

  }
  // tslint:disable-next-line:typedef use-lifecycle-interface
  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

  ngOnDestroy(): void {
    this.webSocketService.closeWebSocket();
  }
  // tslint:disable-next-line:typedef
  sendMessage(sendForm: NgForm) {
    // const chatMessageDto = new ChatMessage(sendForm.value.user, sendForm.value.message);

    const chatMessageDto = new ChatMessage(this.sender, sendForm.value.message);
    this.message.senderId = this.sender;
    // this.message.receiverId = 'testtt receiver' ;
    this.message.receiverId = this.receiver;
    this.message.content = sendForm.value.message;

    this.messageService.sendMessage(this.message).subscribe(data => {console.log(data); }, error => console.log(error));

    this.webSocketService.sendMessage(chatMessageDto);
    // window.location.reload();
    sendForm.controls.message.reset();

  }



  // tslint:disable-next-line:typedef
  getImageLoggedUser() {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.tokenStorageService.getToken()}` });
    this.httpClient.get('http://localhost:8080/api/get/' + this.loggedUser, { headers: header})
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
  }
  // tslint:disable-next-line:typedef
  getImageReceiver() {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.tokenStorageService.getToken()}` });
    this.httpClient.get('http://localhost:8080/api/get/' + this.receiver, { headers: header})
      .subscribe(
        res => {
          this.retrieveResonseReceiver = res;
          this.base64DataReceiver = this.retrieveResonseReceiver.picByte;
          this.retrievedImageReceiver = 'data:image/jpeg;base64,' + this.base64DataReceiver;
        }
      );
  }

  // tslint:disable-next-line:typedef
  getCountUnseen(m: Message): number
  { // @ts-ignore
    let unseen;
    this.messageService.countUnseen(m).subscribe();
    return unseen;
  }

}
