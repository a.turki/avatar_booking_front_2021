import {Component, OnDestroy, OnInit} from '@angular/core';
import {WebSocketService} from '../../services/web-socket.service';
import {NgForm} from '@angular/forms';
import {ChatMessage} from '../../models/chat-message';
import {MessageService} from '../../services/message.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage.service';
import {Mission} from '../../models/mission';
import {Message} from '../../models/message';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {
  sender: string;
  receiver: string;
  message: Message = new Message();
  // tslint:disable-next-line:max-line-length
  constructor(public webSocketService: WebSocketService, private messageService: MessageService, private tokenStorageService: TokenStorageService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    // this.client = this.tokenStorageService.getUser().email;
    this.sender = this.tokenStorageService.getUser().username;
    this.receiver = this.route.snapshot.params.receiver;
    this.webSocketService.openWebSocket(this.sender, this.receiver);
  }

  ngOnDestroy(): void {
    this.webSocketService.closeWebSocket();
  }

  // tslint:disable-next-line:typedef
  sendMessage(sendForm: NgForm) {
   // const chatMessageDto = new ChatMessage(sendForm.value.user, sendForm.value.message);
    const chatMessageDto = new ChatMessage(this.sender, sendForm.value.message);
    this.message.senderId = this.sender;
    // this.message.receiverId = 'testtt receiver' ;
    this.message.receiverId = this.receiver;
    this.message.content = sendForm.value.message;

    this.messageService.sendMessage(this.message).subscribe(data => {console.log(data); }, error => console.log(error));

    this.webSocketService.sendMessage(chatMessageDto);
    sendForm.controls.message.reset();
  }

}
