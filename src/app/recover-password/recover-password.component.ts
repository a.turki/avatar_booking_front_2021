import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PasswordrecoveryService} from '../services/passwordrecovery.service';
import {User} from '../models/user';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.css']
})
export class RecoverPasswordComponent implements OnInit {
  form: any = {};
  email: string;
  password: string;
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  constructor(private route: ActivatedRoute, private passwordRecover: PasswordrecoveryService) { }

  ngOnInit(): void {
    this.email = this.route.snapshot.params.email;
    console.log('*******' + this.email);
    // this.passwordRecover.recoverTokenCreation(this.email).subscribe(data => {console.log(data); }, error => console.log(error));
    console.log('**************** token cree check base');

  }


  onSubmit(): void{
    console.log(this.email + 'recupered mail from input');
    this.form.email = this.route.snapshot.params.email;
    this.form.password = this.password;
    this.passwordRecover.newPwd(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.isSuccessful = false;
        this.isSignUpFailed = true;
        this.errorMessage = err.error.message;

      });
    console.log('clicked');

  }


}
