import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TokenStorageService} from '../../services/token-storage.service';

@Component({
  selector: 'app-navbar-non-logged',
  templateUrl: './navbar-non-logged.component.html',
  styleUrls: ['./navbar-non-logged.component.css']
})
export class NavbarNonLoggedComponent implements OnInit {
  isLoggedIn = false;
  constructor(private httpClient: HttpClient, private tokenStorageService: TokenStorageService) { }
  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
  }

}
