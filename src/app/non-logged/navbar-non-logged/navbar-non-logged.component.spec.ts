import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarNonLoggedComponent } from './navbar-non-logged.component';

describe('NavbarNonLoggedComponent', () => {
  let component: NavbarNonLoggedComponent;
  let fixture: ComponentFixture<NavbarNonLoggedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarNonLoggedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarNonLoggedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
