import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterNonLoggedComponent } from './footer-non-logged.component';

describe('FooterNonLoggedComponent', () => {
  let component: FooterNonLoggedComponent;
  let fixture: ComponentFixture<FooterNonLoggedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FooterNonLoggedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterNonLoggedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
