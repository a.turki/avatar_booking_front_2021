import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer-non-logged',
  templateUrl: './footer-non-logged.component.html',
  styleUrls: ['./footer-non-logged.component.css']
})
export class FooterNonLoggedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
