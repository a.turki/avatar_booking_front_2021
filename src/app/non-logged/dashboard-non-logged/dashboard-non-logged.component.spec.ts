import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardNonLoggedComponent } from './dashboard-non-logged.component';

describe('DashboardNonLoggedComponent', () => {
  let component: DashboardNonLoggedComponent;
  let fixture: ComponentFixture<DashboardNonLoggedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardNonLoggedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardNonLoggedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
