import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../../services/token-storage.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard-non-logged',
  templateUrl: './dashboard-non-logged.component.html',
  styleUrls: ['./dashboard-non-logged.component.css']
})
export class DashboardNonLoggedComponent implements OnInit {
  isLoggedIn = false;
  constructor(private tokenStorageService: TokenStorageService, private router: Router) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
  }

}
