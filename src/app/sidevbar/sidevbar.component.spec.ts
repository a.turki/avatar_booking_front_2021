import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidevbarComponent } from './sidevbar.component';

describe('SidevbarComponent', () => {
  let component: SidevbarComponent;
  let fixture: ComponentFixture<SidevbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidevbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidevbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
