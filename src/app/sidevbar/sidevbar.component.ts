import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../services/token-storage.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sidevbar',
  templateUrl: './sidevbar.component.html',
  styleUrls: ['./sidevbar.component.css']
})
export class SidevbarComponent implements OnInit {
  private roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  showAvatarBoard = false;
  showClientBoard = false;
  username: string;
  // ************photo
  selectedFile: File;
  imgURL: any;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
  constructor(private router: Router, private httpClient: HttpClient, private tokenStorageService: TokenStorageService) { }

  getImage(): void {
    const header = new HttpHeaders({ Authorization: `Bearer ${this.tokenStorageService.getToken()}` });
    this.httpClient.get('http://localhost:8080/api/get/' + this.imageName, { headers: header})
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/png;base64,' + this.base64Data;
        }
      );
  }
  ngOnInit(): void { this.isLoggedIn = !!this.tokenStorageService.getToken();

                     if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      this.showAvatarBoard = this.roles.includes('ROLE_AVATAR');
      this.showClientBoard = this.roles.includes('ROLE_CLIENT');

      this.username = user.username;

      this.imageName = this.tokenStorageService.getUser().username;
     // console.log('photo');
     // console.log('username:' + this.imageName);
      this.getImage();
    }
  }
  goDashboardChart(): void{
    this.router.navigate(['/DashboardChart']);
    window.location.reload();


  }
  logout(): void {
    this.tokenStorageService.signOut();

    window.location.reload();
  }
}
