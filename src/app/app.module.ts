import { BrowserModule } from '@angular/platform-browser';
// @ts-ignore
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { SidevbarComponent } from './sidevbar/sidevbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { BoardModeratorComponent } from './board-moderator/board-moderator.component';
import { BoardUserComponent } from './board-user/board-user.component';
import {authInterceptorProviders} from './helpers/auth.interceptor';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ProfileUpdateComponent } from './profile-update/profile-update.component';
import { PhotoComponent } from './photo/photo.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { RecoverPasswordComponent } from './recover-password/recover-password.component';
import { ReprofileComponent } from './try/reprofile/reprofile.component';
import { ListmissionComponent } from './mission/listmission/listmission.component';
import { UpdatemissionComponent } from './mission/updatemission/updatemission.component';
import { MissiondetailsComponent } from './mission/missiondetails/missiondetails.component';
import { DeletemissionComponent } from './mission/deletemission/deletemission.component';
import { AddmissionComponent } from './mission/addmission/addmission.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatNativeDateModule} from '@angular/material/core';
import { AvatardetailsComponent } from './mission/avatardetails/avatardetails.component';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MyDialogModalComponent } from './modals/my-dialog-modal/my-dialog-modal.component';
import { MyDeleteModalsComponent } from './modals/my-delete-modals/my-delete-modals.component';
import { ConsultmissionComponent } from './mission/consultmission/consultmission.component';
import { AvatarUndoneMissionComponent } from './mission/avatar-undone-mission/avatar-undone-mission.component';
import { MymissionsComponent } from './mission/mymissions/mymissions.component';
import { MissionAvatarsComponent } from './mission/mission-avatars/mission-avatars.component';
import { MissionInformationComponent } from './mission/mission-information/mission-information.component';
import { MissionDetailsComponent } from './modals/mission-details/mission-details.component';
import {AvatarDoingMissionComponent} from './mission/avatar-doing-mission/avatar-doing-mission.component';
import {AvatarDoneMissionComponent} from './mission/avatar-done-mission/avatar-done-mission.component';
import { RateMissionComponent } from './mission/rate-mission/rate-mission.component';
import { RatingModalsComponent } from './modals/rating-modals/rating-modals.component';
import { AvatarAllRatingComponent } from './mission/avatar-all-rating/avatar-all-rating.component';
import { AvatarRatesModalsComponent } from './modals/avatar-rates-modals/avatar-rates-modals.component';
import { AddSpecificMissionComponent } from './mission/add-specific-mission/add-specific-mission.component';
import { AddSpecificMissionSelectAvatarsComponent } from './mission/add-specific-mission-select-avatars/add-specific-mission-select-avatars.component';
import { SpecificMissionModalComponent } from './modals/specific-mission-modal/specific-mission-modal.component';
import { ChatComponent } from './chat/chat/chat.component';
import { DiscussionComponent } from './chat/discussion/discussion.component';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import { MissionDetails2Component } from './mission/mission-details2/mission-details2.component';
import { CreateOfferComponent } from './offer/create-offer/create-offer.component';
import { ConsultOfferComponent } from './offer/consult-offer/consult-offer.component';
import {JwPaginationModule} from 'jw-angular-pagination';
import { NotificationComponent } from './notification/notification.component';
import { DashboardNonLoggedComponent } from './non-logged/dashboard-non-logged/dashboard-non-logged.component';
import { NavbarNonLoggedComponent } from './non-logged/navbar-non-logged/navbar-non-logged.component';
import { FooterNonLoggedComponent } from './non-logged/footer-non-logged/footer-non-logged.component';
import { AvatarMissionProgressComponent } from './mission/avatar-mission-progress/avatar-mission-progress.component';
import { PaginationControlsComponent } from './pagination-controls/pagination-controls.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { SpecificAvatarcardPhotoComponent } from './mission/specific-avatarcard-photo/specific-avatarcard-photo.component';
import {ImageCropperModule} from 'ngx-image-cropper';
import { ChatRoomComponent } from './chat/chat-room/chat-room.component';
import { ChatDiscussionComponent } from './chat/chat-discussion/chat-discussion.component';
import { ChatPhotoComponent } from './chat/chat-photo/chat-photo.component';
import { NavbarNotifPhotoComponent } from './mission/navbar-notif-photo/navbar-notif-photo.component';
import {BarRatingModule} from 'ngx-bar-rating';
import { RedesignComponent } from './modals/redesign/redesign.component';
import { BoadcastMissionModalComponent } from './modals/boadcast-mission-modal/boadcast-mission-modal.component';
import { ProfileVisitComponent } from './profile-visit/profile-visit.component';
import { MakeOfferComponent } from './mission/make-offer/make-offer.component';
import { MakeOfferModalComponent } from './modals/make-offer-modal/make-offer-modal.component';
import { AddCountryComponent } from './admin/add-country/add-country.component';
import { ListCountryComponent } from './admin/list-country/list-country.component';
import { ListRegionComponent } from './admin/list-region/list-region.component';
import { AddRegionComponent } from './admin/add-region/add-region.component';
import {ToastrModule} from 'ngx-toastr';
import {GoogleChartsModule} from 'angular-google-charts';
import { Charts1Component } from './admin/charts1/charts1.component';
import { Chart2Component } from './admin/chart2/chart2.component';
import {CdTimerModule} from 'angular-cd-timer';
import { ReclamationModalsComponent } from './modals/reclamation-modals/reclamation-modals.component';
import { AddReclamationComponent } from './reclamation/add-reclamation/add-reclamation.component';
import { DashboardChartsComponent } from './admin/dashboard-charts/dashboard-charts.component';
import { ListReclamationComponent } from './reclamation/list-reclamation/list-reclamation.component';
import { Addmission2Component } from './mission/addmission2/addmission2.component';
import { NavComponent } from './HomePage/nav/nav.component';
import { PageComponent } from './HomePage/page/page.component';
import { HistoriqueReclamationComponent } from './reclamation/historique-reclamation/historique-reclamation.component';
import { DeleteClaimModalComponent } from './modals/delete-claim-modal/delete-claim-modal.component';
import { DeleteReclamationComponent } from './reclamation/delete-reclamation/delete-reclamation.component';




@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    SidevbarComponent,
    DashboardComponent,
    LandingpageComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    BoardAdminComponent,
    BoardModeratorComponent,
    BoardUserComponent,
    ProfileUpdateComponent,
    PhotoComponent,
    ResetPasswordComponent,
    RecoverPasswordComponent,
    ReprofileComponent,
    ListmissionComponent,
    UpdatemissionComponent,
    MissiondetailsComponent,
    DeletemissionComponent,
    AddmissionComponent,
    AvatardetailsComponent,
    MyDialogModalComponent,
    MyDeleteModalsComponent,
    ConsultmissionComponent,
    AvatarUndoneMissionComponent,
    MymissionsComponent,
    MissionAvatarsComponent,
    MissionInformationComponent,
    MissionDetailsComponent,
    AvatarDoingMissionComponent,
    AvatarDoneMissionComponent,
    RateMissionComponent,
    RatingModalsComponent,
    AvatarAllRatingComponent,
    AvatarRatesModalsComponent,
    AddSpecificMissionComponent,
    AddSpecificMissionSelectAvatarsComponent,
    SpecificMissionModalComponent,
    ChatComponent,
    DiscussionComponent,
    MissionDetails2Component,
    CreateOfferComponent,
    ConsultOfferComponent,
    NotificationComponent,
    DashboardNonLoggedComponent,
    NavbarNonLoggedComponent,
    FooterNonLoggedComponent,
    AvatarMissionProgressComponent,
    PaginationControlsComponent,
    SpecificAvatarcardPhotoComponent,
    ChatRoomComponent,
    ChatDiscussionComponent,
    ChatPhotoComponent,
    NavbarNotifPhotoComponent,
    RedesignComponent,
    BoadcastMissionModalComponent,
    ProfileVisitComponent,
    MakeOfferComponent,
    MakeOfferModalComponent,
    AddCountryComponent,
    ListCountryComponent,
    ListRegionComponent,
    AddRegionComponent,
    Charts1Component,
    Chart2Component,
    ReclamationModalsComponent,
    AddReclamationComponent,
    DashboardChartsComponent,
    ListReclamationComponent,
    Addmission2Component,
    NavComponent,
    PageComponent,
    HistoriqueReclamationComponent,
    DeleteClaimModalComponent,
    DeleteReclamationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
    NgbModule,
    BrowserModule,
    Ng2SearchPipeModule,
    MatFormFieldModule,
    JwPaginationModule,
    NgxPaginationModule,
    ImageCropperModule,
    BarRatingModule,
    ToastrModule.forRoot(),
    CdTimerModule,
    GoogleChartsModule
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
